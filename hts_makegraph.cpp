#include <Windows.h>
#undef min, max
#undef CreateWindow
#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <cstdint>
#include <numeric>
#include <algorithm>
#include <direct.h>
#include <iomanip>

#include <picojson.h>

#include <TH2D.h>
#include <TF2.h>
#include <TAxis.h>
#include <TObjArray.h>
#include <TObjString.h>
#include <TString.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TH1D.h>
#include <TSystem.h>
#include <TPaveStats.h>
#include <TPad.h>
#include <TArrow.h>
#include <TPolyLine.h>
#include <TGaxis.h>
#include <YoshimotoStyle.hpp>
#include <JsonIO.hpp>
#include <DataIO.hpp>
#include <MyTrackStruct.hpp>
#include <MyMath.hpp>
#include <omp.h>
#include <BetaIO.hpp>
#if _DEBUG
#include <crtdbg.h>
#define _CRTDBG_MAP_ALLOC
#define new  ::new(_NORMAL_BLOCK, __FILE__, __LINE__)
#endif
using namespace std;
using namespace phst;
using namespace phst::base;
string ofolder;
string ifolder;
namespace
{
    void refference_arrow(double r, string txt, double x0 = 0, double y0 = 0)
    {
        double xmin = -15 + x0, ymin = -10 + y0;
        TPaveText* pt = new TPaveText(xmin + 1, ymin + 1, xmin + 10, ymin + 5);
        pt->SetFillColor(0);
        pt->SetLineColor(0);
        pt->SetShadowColor(0);
        TText* text = pt->AddText(txt.c_str());
        pt->Draw();
        // Refference
        {
            TArrow* arrow = new TArrow(xmin, ymin, xmin + 1 * r, ymin, 0.003, ">");
            arrow->SetFillColor(1);
            arrow->SetLineColor(1);
            arrow->SetFillStyle(1001);
            arrow->SetLineWidth(1);
            arrow->SetLineStyle(1);
            arrow->Draw();
        }
        {
            TArrow* arrow = new TArrow(xmin, ymin, xmin, ymin + 1 * r, 0.003, ">");
            arrow->SetFillColor(1);
            arrow->SetLineColor(1);
            arrow->SetFillStyle(1001);
            arrow->SetLineWidth(1);
            arrow->SetLineStyle(1);
            arrow->Draw();
        }
    }

    int TotalView;
    int TotalImager;
    int TotalImagerView;

    class Summary
    {
        double rate_out_emul[2];
        double rate_edge_emulay[2];
        double rate_lack_pic[2];
        double ave_nog[2];
        double ave_not[2];
        double in_emul[2];
    public:
        double all_emul[2];
        double out_emul[2];
        double edge_up[2];
        double edge_down[2];
        double lack_pic[2];
        double dark_emul[2];
        double effective_emullay[2];
        double sum_nog[2];
        double sum_nog0[2];
        double sum_nog15[2];
        double sum_not[2];
        void show()
        {
            for (int i = 0; i < 2; i++)
            {
                cout << "Layer: " << i << endl;
                if (all_emul[i] == 0)
                {
                    cout << "0 views scanned" << endl;
                    continue;
                }
                cout << fixed << std::setprecision(2);
                cout << "Rate of out of emulsion [%]: " << out_emul[i] / all_emul[i] * 100 << endl;
                in_emul[i] = all_emul[i] - out_emul[i];
                if (in_emul[i] == 0)
                {
                    cout << "0 views found" << endl;
                    continue;
                }
                cout << "Rate of on up edge      [%]: " << edge_up[i] / in_emul[i] * 100 << endl;
                cout << "Rate of on down edge    [%]: " << edge_down[i] / in_emul[i] * 100 << endl;
                cout << "Rate of dark view       [%]: " << dark_emul[i] / in_emul[i] * 100 << endl;
                cout << "Rate of lack of pic     [%]: " << lack_pic[i] / in_emul[i] * 100 << endl;
                cout << fixed << std::setprecision(0);
                cout << "Number of grains /view     : " << sum_nog[i] / in_emul[i] << endl;
                cout << fixed << std::setprecision(2);
                cout << "Rate of 1 st NOG < ave/2[%]: " << sum_nog0[i] / in_emul[i] * 100 << endl;
                cout << "Rate of 16th NOG < ave/2[%]: " << sum_nog15[i] / in_emul[i] * 100 << endl;
                cout << fixed << std::setprecision(0);
                cout << "Number of tracks /view     : " << sum_not[i] / in_emul[i] << endl;
            }
        }
        void save(string str)
        {
            FILE* fp_out = freopen(str.c_str(), "w", stdout);
            if (fp_out == NULL)
            {
                return;
            }
            show();
            fclose(fp_out);
        }
    };
    static string GetAxisTitle(string path1, string path2) {
        if (path1 == "ScanEachLayerParam") { return path2; }
        if (path1 == "ImagerControllerParam") { return path2; }
        if (path1 == "SurfaceSummary") { return path2; }
        if (path1 == "Nogs") { return "# of grains"; }
        return path1;
    }
    class Graph
    {
    public:
        Graph() {}
    private:
        picojson::array ScanLinesOrigin;
        picojson::array Affine;
        double min_sx, max_sx, min_sy, max_sy;
        double min_ix, max_ix, min_iy, max_iy;
        int Nview_x, Nview_y;
        int Nimagerview_x, Nimagerview_y;
        vector<double> view_x;
        vector<double> view_y;
        vector<int> vlayer;

        vector<double> imagerview_x;
        vector<double> imagerview_y;

        vector<double> affine_x;
        vector<double> affine_y;

        vector<double> stage_x;
        vector<double> stage_y;
        vector<double> stage_x0;
        vector<double> stage_y0;
        vector<int> stage_layer;
        vector<int> view_flag;
    public:
        double GetMin_sx()const { return min_sx; }
        double GetMin_sy()const { return min_sy; }
        static double GetAlignedImagerView_X(double sx, double ix)
        {
            return sx + double(std::round((ix) * 6.0 / 5.0)) * 5.0 / 6.0;
        }
        static double GetAlignedImagerView_Y(double sx, double ix)
        {
            return sx + double(std::round((+ix) * 12.0 / 5.0)) * 5.0 / 12.0;
        }
        // ScanLinesOriginからView用TH2Dに詰めるためのパラメータを計算
        void SetScanLinesOrigin(picojson::array ScanLinesOrigin)
        {
            this->ScanLinesOrigin = ScanLinesOrigin;

            for (auto p = ScanLinesOrigin.begin(); p != ScanLinesOrigin.end(); p++)
            {
                auto q = p->get<picojson::object>();
                double X = q.at("X").get<double>();
                double Y = q.at("Y").get<double>();
                double Layer = q.at("Layer").get<double>();
                view_x.push_back(X);
                view_y.push_back(Y);
                vlayer.push_back(Layer);
            }
            //重複を削除
            std::sort(view_x.begin(), view_x.end());
            view_x.erase(unique(view_x.begin(), view_x.end()), view_x.end());
            std::sort(view_y.begin(), view_y.end());
            view_y.erase(unique(view_y.begin(), view_y.end()), view_y.end());
            std::sort(vlayer.begin(), vlayer.end());
            vlayer.erase(unique(vlayer.begin(), vlayer.end()), vlayer.end());

            min_sx = view_x.front();
            min_sy = view_y.front();
            max_sx = (int(std::floor(view_x.back() - min_sx)) / 5) * 5 + 5 + min_sx;
            max_sy = (int(std::floor(view_y.back() - min_sy)) / 5) * 5 + 5 + min_sy;

            min_sx += -2.5;
            min_sy += -2.5;
            max_sx += -2.5;
            max_sy += -2.5;

            Nview_x = view_x.size();
            Nview_y = view_y.size();
        }
        // AffineからImagerView用TH2Dに詰めるためのパラメータを計算
        void SetAffine(picojson::array Affine, string direction = "")
        {
            this->Affine = Affine;
            for (int i = 0; i < Affine.size(); i++)
            {
                picojson::array aff_coef = Affine.at(i).get<picojson::object>()["Aff_coef"].get<picojson::array>();
                double ix = aff_coef.at(4).get<double>();
                double iy = aff_coef.at(5).get<double>();
                affine_x.push_back(ix);
                affine_y.push_back(iy);
            }

            for (auto p = ScanLinesOrigin.begin(); p != ScanLinesOrigin.end(); p++)
            {
                auto q = p->get<picojson::object>();
                double sx = q.at("X").get<double>();
                double sy = q.at("Y").get<double>();
                int layer = int(q.at("Layer").get<double>());
                if (q.at("Param").is<picojson::object>() && direction != "")
                {
                    picojson::object param = q.at("Param").get<picojson::object>();
                    if (!param.empty())
                    {
                        string d = param.at("Direction").get<string>();
                        if (d == direction) { view_flag.push_back(1); }
                        else { view_flag.push_back(0); }
                    }
                }
                else { view_flag.push_back(1); }

                stage_x.push_back(sx);
                stage_y.push_back(sy);
                stage_layer.push_back(layer);
                if (layer == 0)
                {
                    stage_x0.push_back(sx);
                    stage_y0.push_back(sy);
                }
                for (int i = 0; i < Affine.size(); i++)
                {
                    imagerview_x.push_back(GetAlignedImagerView_X(stage_x.back(), affine_x.at(i)));
                    imagerview_y.push_back(GetAlignedImagerView_Y(stage_y.back(), affine_y.at(i)));
                }
            }
            std::sort(imagerview_x.begin(), imagerview_x.end());
            imagerview_x.erase(unique(imagerview_x.begin(), imagerview_x.end()), imagerview_x.end());

            std::sort(imagerview_y.begin(), imagerview_y.end());
            imagerview_y.erase(unique(imagerview_y.begin(), imagerview_y.end()), imagerview_y.end());

            min_ix = imagerview_x.front();
            min_iy = imagerview_y.front();

            max_ix = (int(std::floor(imagerview_x.back() - min_ix)) / 5) * 5 + 5 + min_ix;
            max_iy = (int(std::floor(imagerview_y.back() - min_iy)) / 5) * 5 + 5 + min_iy;

            min_ix += -5.0 / 6.0 / 2.0;
            min_iy += -5.0 / 6.0 / 2.0;
            max_ix += -5.0 / 12.0 / 2.0;
            max_iy += -5.0 / 12.0 / 2.0;

            Nimagerview_x = imagerview_x.size();
            Nimagerview_y = imagerview_y.size();
        }
        void SetViewHist(vector<TH2D*>& vh2, string pre_name)
        {
            string strs[] = { " Layer 0"," Layer 1" };
            for (int i = 0; i < vlayer.size(); i++)
            {
                auto h2 = new TH2D((pre_name + "_h2_" + std::to_string(i)).c_str(), (pre_name + strs[i]).c_str(), Nview_x, min_sx, max_sx, Nview_y, min_sy, max_sy);
                h2->SetXTitle("X [mm]");
                h2->SetYTitle("Y [mm]");
                vh2.push_back(h2);
            }
        }
        void SetImagerViewHist(vector<TH2D*>& vh2, string pre_name)
        {
            string strs[] = { " Layer 0"," Layer 1" };
            for (int i = 0; i < vlayer.size(); i++)
            {
                auto h2 = new TH2D((pre_name + "_h2_" + std::to_string(i)).c_str(), (pre_name + strs[i]).c_str(), Nimagerview_x, min_ix, max_ix, Nimagerview_y, min_iy, max_iy);
                h2->SetXTitle("X [mm]");
                h2->SetYTitle("Y [mm]");
                vh2.push_back(h2);
            }
        }
        void SetSimpleHist(vector<TH1D*>& vh1, string path1, string path2, int nbin, double xlow, double xup)
        {
            for (int i = 0; i < vlayer.size(); i++)
            {
                auto h2 = new TH1D((path1 + "_h1_" + std::to_string(i)).c_str(), "", nbin, xlow, xup);
                h2->GetXaxis()->SetTitle(GetAxisTitle(path1, path2).c_str());
                h2->GetYaxis()->SetTitle("Frequency");
                vh1.push_back(h2);
            }
        }
        TH2D* SetImagerViewHist(string pre_name)
        {
            auto h2 = new TH2D((pre_name + "_h2").c_str(), "", Nimagerview_x, min_ix, max_ix, Nimagerview_y, min_iy, max_iy);
            h2->SetXTitle("X [mm]");
            h2->SetYTitle("Y [mm]");
            return h2;
        }
        void SetBin(int view, double value, vector<TH2D*>& vh2)
        {
            if (view_flag.at(view) == 0) { return; }
            size_t bin_x = find(view_x.begin(), view_x.end(), stage_x.at(view)) - view_x.begin() + 1;
            size_t bin_y = find(view_y.begin(), view_y.end(), stage_y.at(view)) - view_y.begin() + 1;
            vh2[stage_layer.at(view)]->SetBinContent(bin_x, bin_y, value);
        }
        void GetBinDiff(int view, const vector<TH2D*>& vh2, double& x, double& y)
        {
            size_t bin_x = find(view_x.begin(), view_x.end(), stage_x.at(view)) - view_x.begin() + 1;
            size_t bin_y = find(view_y.begin(), view_y.end(), stage_y.at(view)) - view_y.begin() + 1;
            x = 0, y = 0;
            auto p = vh2[stage_layer.at(view)];
            int numx = 0, numy = 0;
            if (bin_x - 1 > 0)
            {
                x += (p->GetBinContent(bin_x, bin_y) - p->GetBinContent(bin_x - 1, bin_y)) / (view_x[bin_x - 1] - view_x[bin_x - 2]);
                numx++;
            }
            if (bin_x + 1 <= view_x.size())
            {
                x += (p->GetBinContent(bin_x + 1, bin_y) - p->GetBinContent(bin_x, bin_y)) / (view_x[bin_x - 0] - view_x[bin_x - 1]);
                numx++;
            }
            if (bin_y - 1 > 0)
            {
                y += (p->GetBinContent(bin_x, bin_y) - p->GetBinContent(bin_x, bin_y - 1)) / (view_y[bin_y - 1] - view_y[bin_y - 2]);
                numy++;
            }
            if (bin_y + 1 <= view_y.size())
            {
                y += (p->GetBinContent(bin_x, bin_y + 1) - p->GetBinContent(bin_x, bin_y)) / (view_y[bin_y - 0] - view_y[bin_y - 1]);
                numy++;
            }
            x /= numx;
            y /= numy;
        }
        void SetBin(int view, int imager, double value, vector<TH2D*>& vh2)
        {
            if (view_flag.at(view) == 0) { return; }
            size_t bin_x = find(imagerview_x.begin(), imagerview_x.end(), GetAlignedImagerView_X(stage_x.at(view), affine_x.at(imager))) - imagerview_x.begin() + 1;
            size_t bin_y = find(imagerview_y.begin(), imagerview_y.end(), GetAlignedImagerView_Y(stage_y.at(view), affine_y.at(imager))) - imagerview_y.begin() + 1;
            vh2[stage_layer.at(view)]->SetBinContent(bin_x, bin_y, value);
        }
        void SetBin0(int view, int imager, double value, TH2D* h2)
        {
            if (view_flag.at(view) == 0) { return; }
            size_t bin_x = find(imagerview_x.begin(), imagerview_x.end(), GetAlignedImagerView_X(stage_x0.at(view), affine_x.at(imager))) - imagerview_x.begin() + 1;
            size_t bin_y = find(imagerview_y.begin(), imagerview_y.end(), GetAlignedImagerView_Y(stage_y0.at(view), affine_y.at(imager))) - imagerview_y.begin() + 1;
            h2->SetBinContent(bin_x, bin_y, value);
        }
        int GetBin(int view, int imager)
        {
            size_t bin_x = find(imagerview_x.begin(), imagerview_x.end(), GetAlignedImagerView_X(stage_x.at(view), affine_x.at(imager))) - imagerview_x.begin();
            size_t bin_y = find(imagerview_y.begin(), imagerview_y.end(), GetAlignedImagerView_Y(stage_y.at(view), affine_y.at(imager))) - imagerview_y.begin();

            return bin_x + imagerview_x.size() * bin_y;
        }
        int GetLayer(int view) const
        {
            return stage_layer[view];
        }
        double GetStage(int view, int AX)const
        {
            if (AX == 0) { return stage_x[view]; }
            else if (AX == 1) { return stage_y[view]; }
            else { throw std::exception(); }
        }
        bool GetViewFlag(int view)const
        {
            return view_flag.at(view) == 1 ? true : false;
        }
    };
    class MakeGraph
    {
        Graph graph;
        const picojson::array ScanLinesOrigin, ViewHistory, Affine, EachView;
        picojson::array Results2;
        const int window_w, window_h;
        vector<vector<double>> velapsed;
        vector<vector<double>> vnumberoftracks;
        vector<vector<double>> vnumberoftracks_unclustered;
    public:
        vector<vector<pair<double, double>>> vangles;
        vector<int> vangleviews;
    private:
        vector<vector<pair<double, double>>> vphvol;
        vector<int> vphvolviews;
        int NImager, NView, NImagerView, Nlayer, NRealView;
        Summary summary;
    public:
        MakeGraph() :ScanLinesOrigin(NULL), Affine(NULL), ViewHistory(NULL), window_w(NULL), window_h(NULL) {}
        MakeGraph(picojson::array ScanLinesOrigin, picojson::array Affine, picojson::array ViewHistory, picojson::array EachView,
            int window_w, int window_h, string direction = "") :
            ScanLinesOrigin(ScanLinesOrigin), Affine(Affine),
            ViewHistory(ViewHistory), EachView(EachView), window_w(window_w), window_h(window_h)
        {
            NImager = Affine.size();
            NView = ScanLinesOrigin.size();
            NImagerView = NImager * NView;
            NRealView = ViewHistory.size();
            Nlayer = 2;

            graph.SetScanLinesOrigin(ScanLinesOrigin);
            graph.SetAffine(Affine, direction);
        }
        static void normalize(TH2D* h2) {
            TF2* f2 = new TF2("f2", "[0]+[1]*x+[2]*y",
                ((TAxis*)(h2->GetXaxis()))->GetXmin(),
                ((TAxis*)(h2->GetXaxis()))->GetXmax(),
                ((TAxis*)(h2->GetYaxis()))->GetXmin(),
                ((TAxis*)(h2->GetYaxis()))->GetXmax());
            f2->SetParameter(0, h2->GetMean());
            f2->SetParameter(2, 1);
            f2->SetParameter(3, 2);
            h2->Fit("f2");
            double Xmin = ((TAxis*)(h2->GetXaxis()))->GetXmin();
            double Xmax = ((TAxis*)(h2->GetXaxis()))->GetXmax();
            double Ymin = ((TAxis*)(h2->GetYaxis()))->GetXmin();
            double Ymax = ((TAxis*)(h2->GetYaxis()))->GetXmax();
            cout << h2->GetMean() << endl;
            double a = f2->GetParameter(0);
            double b = f2->GetParameter(1);
            double c = f2->GetParameter(2);
            int NbinX = h2->GetNbinsX();
            int NbinY = h2->GetNbinsY();

            for (int y = 0; y < NbinY; y++) {
                for (int x = 0; x < NbinX; x++) {
                    double Xexp = b * ((TAxis*)(h2->GetXaxis()))->GetBinCenter(x + 1);
                    double Yexp = b * ((TAxis*)(h2->GetYaxis()))->GetBinCenter(y + 1);
                    double Exp = Xexp + Yexp + a;
                    cout << h2->GetBinContent(x + 1, y + 1) << " " << Exp << " " << h2->GetBinContent(x + 1, y + 1) - Exp << endl;
                }
            }
        }
        void getnotstream(ifstream& ifs, int cam, int sen) {
            string path1;
            string path2;
            stringstream ss;

            ss << ofolder << "/NOT/" << std::setw(2) << std::setfill('0') << cam << "_" << std::setw(2) << std::setfill('0') << sen << "_TrackHit2_0_99999999_0_000.txt";
            path2 = ss.str();
            ifs.open(path2);
            if (ifs) {
                return;
            }

            path1 = ifolder + "DATA/" + std::to_string(cam) + "_" + std::to_string(sen) + "/TrackHit2_0_99999999_0_0.txt";
            ifs.open(path1);
            if (!ifs) {
                ss.str(""); ss.clear(stringstream::goodbit);
                ss << ifolder << "DATA/" << std::setw(2) << std::setfill('0') << cam << "_" << std::setw(2) << std::setfill('0') << sen << "/TrackHit2_0_99999999_0_000.txt";
                path1 = ss.str();
                ifs.open(path1);
                if (!ifs) {
                    return;
                }
            }
            ifstream source(path1, ios::binary);
            ofstream dest(path2, ios::binary);
            dest << source.rdbuf();
            source.close();
            dest.close();
        }
        void trygetnot()
        {
            try
            {
                vector < vector<double> >vvd_not;
                vector < vector<double> >vvd_not_unclustered;
                vector < vector<double> >vvd_elapsed;
                mkdir((ofolder + "/NOT").c_str());
                for (int cam = 0; cam < 6; cam++)
                {
                    for (int sen = 0; sen < 12; sen++)
                    {
                        vector<double> vd_not;
                        vector<double> vd_not_unclustered;
                        vector<double> vd_elapsed;
                        ifstream ifs;
                        getnotstream(ifs, cam, sen);
                        if (!ifs) { return; }
                        string str;
                        while (std::getline(ifs, str))
                        {
                            stringstream ss(str);
                            double view, not, elapsed, not_unclustered;
                            ss >> view >> not>> elapsed >> not_unclustered;
                            vd_not.emplace_back(not);
                            vd_not_unclustered.emplace_back(not_unclustered);
                            vd_elapsed.emplace_back(elapsed);
                        }
                        vvd_not.emplace_back(vd_not);
                        vvd_not_unclustered.emplace_back(vd_not_unclustered);
                        vvd_elapsed.emplace_back(vd_elapsed);
                    }
                }
                for (int view = 0; view < vvd_not.at(0).size(); view++)
                {
                    vector<double> vd_not;
                    vector<double> vd_not_unclustered;
                    vector<double> vd_elapsed;
                    for (int imager = 0; imager < 72; imager++)
                    {
                        vd_not.emplace_back(vvd_not.at(imager).at(view));
                        vd_not_unclustered.emplace_back(vvd_not_unclustered.at(imager).at(view));
                        vd_elapsed.emplace_back(vvd_elapsed.at(imager).at(view));
                    }
                    vnumberoftracks.emplace_back(vd_not);
                    vnumberoftracks_unclustered.emplace_back(vd_not_unclustered);
                    velapsed.emplace_back(vd_elapsed);
                }
            }
            catch (std::exception& ex)
            {
                cerr << ex.what() << endl;
                return;
            }
            cout << "trygetnot done." << endl;
        }
        void trygetnot(const std::vector<uint64_t>& vlist)
        {
            try
            {
                for (int view = 0; view < NView; view++) {
                    vector<double> buf, buf_unclusterd;
                    for (int imager = 0; imager < NImager; imager++) {
                        buf.emplace_back(vlist.at(view * NImager + imager));
                        buf_unclusterd.emplace_back(0);
                    }
                    vnumberoftracks.emplace_back(buf);
                    vnumberoftracks_unclustered.emplace_back(buf_unclusterd);
                }
            }
            catch (std::exception& ex)
            {
                cerr << ex.what() << endl;
                return;
            }
            cout << "trygetnot done." << endl;
        }
        void makegraph3(TCanvas* can, string oname, string path1, string path2, double xlow, double xup)
        {
            vector<TH2D*> vh2_1;
            vector<TH2D*> vh2_2;
            TH2D* h2 = graph.SetImagerViewHist(path1);
            graph.SetImagerViewHist(vh2_1, path1 + ((path2 != "") ? " " + path2 : ""));
            graph.SetViewHist(vh2_2, path1 + ((path2 != "") ? " " + path2 : "") + "_2");

            vector<double> vvthick(NView / 2 * NImager);
            vector<double> Sum(2, 0);
            int view0 = 0;
            try
            {
                vector<int> mimager(NView / 2 * NImager);
                vector<int> mview(NView / 2 * NImager);
                //各ビンに詰める
                for (int i = 0; i < NView; i++)
                {
                    auto scan = &ScanLinesOrigin.at(i);
                    auto s = scan->get<picojson::object>();
                    int layer = graph.GetLayer(i);

                    vector<double> arr;
                    getarray(arr, i, path1, path2);
                    double ave = std::accumulate(arr.begin(), arr.end(), 0.0) / arr.size();
                    for (int j = 0; j < NImager; j++)
                    {
                        double value = arr.at(j);
                        Sum.at(layer) += value;
                        graph.SetBin(i, j, value, vh2_1);
                        int k = graph.GetBin(i, j);
                        if (graph.GetViewFlag(i)) { vvthick.at(k) += (value * (layer == 0 ? 1.0 : -1.0)); }
                        if (layer == 0)
                        {
                            mimager.at(k) = j;
                            mview.at(k) = view0;
                        }
                    }
                    if (layer == 0) { view0++; }
                    graph.SetBin(i, ave, vh2_2);
                }
                //平均値を求める
                Sum.at(0) /= (NView * NImager / 2);
                Sum.at(1) /= (NView * NImager / 2);
                double Ave = Sum.at(0) - Sum.at(1);
                //差をビンに詰める
                TH1D* h1 = new TH1D(("h2_" + path1).c_str(), "", 25, Ave + xlow, Ave + xup);
                for (int k = 0; k < NView / 2 * NImager; k++)
                {
                    graph.SetBin0(mview.at(k), mimager.at(k), vvthick.at(k), h2);
                    h1->Fill(vvthick.at(k));
                }
                make3p2graph(can, oname, vh2_1, vh2_2, h1, h2);
                delete h1;
            }
            catch (std::exception& ex)
            {
                cerr << ex.what() << endl;
                cerr << "Error at makegraph3 of " << path1 << " & " << path2 << endl;
            }

            for (int i = 0; i < vh2_1.size(); i++)
            {
                delete vh2_1[i];
            }
            vh2_1.clear();
            vh2_1.shrink_to_fit();
            for (int i = 0; i < vh2_2.size(); i++)
            {
                delete vh2_2[i];
            }
            vh2_2.clear();
            vh2_2.shrink_to_fit();

            delete h2;
        }
        void makegraph1(TCanvas* can, string oname, string path1, string path2, double xlow, double xup, int nbin, int pages, bool textflag = false)
        {
            vector<TH2D*> vh2_1;
            vector<TH1D*> vh1_1;

            graph.SetViewHist(vh2_1, path1 + ((path2 != "") ? " " + path2 : ""));
            getbin(path1, path2, nbin, xlow, xup);
            graph.SetSimpleHist(vh1_1, path1, path2, nbin, xlow, xup);
            vector<vector<double>> vsum;
            for (int i = 0; i < 2; i++)
            {
                vector<double> sum(NView / 2, 0);
                vsum.push_back(sum);
            }

            try
            {
                vector<int> vint(2);
                for (int i = 0; i < NView; i++)
                {
                    if (i >= NRealView) { break; }
                    int layer = graph.GetLayer(i);
                    double value = 0;
                    if (path2.substr(0, 7) == "Process")
                    {
                        istringstream iss(path2);
                        string str;
                        int j;
                        iss >> str >> j;
                        getvalue_process(value, i, j);
                    }
                    else
                    {
                        getvalue(value, i, path1, path2);
                    }
                    graph.SetBin(i, value, vh2_1);
                    if (graph.GetViewFlag(i)) { vh1_1[layer]->Fill(value); }
                    if (graph.GetViewFlag(i)) { vsum.at(layer).at(vint.at(layer)) = value; }
                    vint.at(layer)++;
                }
                double vlow[] = { xlow,xlow };
                double vup[] = { xup,xup };
                double vave[] = { 0,0 };
                if (pages == 4)
                {
                    make4graph(can, oname, vh2_1, vh1_1, nbin, vlow, vup, path1, path2);
                }
                else if (pages == 6)
                {
                    make6graph(can, oname, vh2_1, vh1_1, vsum, nbin, vlow, vup, vave, path1, path2, "views", textflag);
                }
            }
            catch (...) { cerr << "Error at makegraph1 of " << path1 << " & " << path2 << endl; }
            for (int i = 0; i < vh2_1.size(); i++)
            {
                delete vh2_1[i];
            }
            vh2_1.clear();
            vh2_1.shrink_to_fit();
            for (int i = 0; i < vh1_1.size(); i++)
            {
                delete vh1_1[i];
            }
            vh1_1.clear();
            vh1_1.shrink_to_fit();
        }
        void makegraph2(TCanvas* can, string oname, string path1, string path2, double xlow, double xup, int pages)
        {
            vector<TH2D*> vh2_1;
            vector<TH1D*> vh1_1;
            graph.SetImagerViewHist(vh2_1, path1 + ((path2 != "") ? " " + path2 : ""));
            int nbin = 50;
            getbin(path1, path2, nbin, xlow, xup);
            graph.SetSimpleHist(vh1_1, path1, path2, nbin, xlow, xup);
            vector<vector<double>> vsum;
            for (int i = 0; i < 2; i++)
            {
                vector<double> sum(NImager, 0);
                vsum.push_back(sum);
            }
            try
            {
                vector<int> vint(2);
                //各ビンに詰める
                for (int i = 0; i < NView; i++)
                {
                    if (i >= NRealView) { break; }
                    auto scan = &ScanLinesOrigin.at(i);
                    auto s = scan->get<picojson::object>();
                    int layer = graph.GetLayer(i);

                    vector<double> arr;
                    getarray(arr, i, path1, path2);
                    for (int j = 0; j < NImager; j++)
                    {
                        double value = arr.at(j);
                        if (graph.GetViewFlag(i)) { vsum.at(layer).at(j) += value; }
                        graph.SetBin(i, j, value, vh2_1);
                        if (graph.GetViewFlag(i)) { vh1_1[layer]->Fill(value); }
                    }
                    vint.at(layer)++;
                }
                //平均値を求める
                for (int i = 0; i < 2; i++)
                {
                    for (int j = 0; j < NImager; j++)
                    {
                        vsum[i][j] /= double(vint[i]);
                    }
                }

                double vlow[] = { xlow,xlow };
                double vup[] = { xup,xup };
                double vave[] = { 0,0 };
                if (pages == 6)
                {
                    make6graph(can, oname, vh2_1, vh1_1, vsum, nbin, vlow, vup, vave, path1, path2, "sensors", false);
                }
                else if (pages == 4)
                {
                    make4graph(can, oname, vh2_1, vh1_1, nbin, vlow, vup, path1, path2);
                }
            }
            catch (...) { cerr << "Error at makegraph2 of " << path1 << " & " << path2 << endl; }
            for (int i = 0; i < vh2_1.size(); i++)
            {
                delete vh2_1[i];
            }
            vh2_1.clear();
            vh2_1.shrink_to_fit();
            for (int i = 0; i < vh1_1.size(); i++)
            {
                delete vh1_1[i];
            }
            vh1_1.clear();
            vh1_1.shrink_to_fit();
        }
        void makegraph_angle(TCanvas* can, string oname, double xlow, double xup, int nbin, int phcut = 0)
        {
            vector<TH2D*> vh2_2;
            vector<TH1D*> vh1_x;
            vector<TH1D*> vh1_y;
            for (int i = 0; i < Nlayer; i++)
            {
                auto h2 = new TH2D(("2D" + std::to_string(i)).c_str(), "", nbin, xlow, xup, nbin, xlow, xup);
                h2->SetXTitle("Shift ax [#mum]");
                h2->SetYTitle("Shift ay [#mum]");
                h2->SetTitle(("Shift layer " + std::to_string(i)).c_str());
                vh2_2.push_back(h2);
                auto h1_x = new TH1D(("1D_x" + std::to_string(i)).c_str(), "", nbin, xlow, xup);
                h1_x->SetXTitle("Shift ax [#mum]");
                h1_x->SetTitle(("Shift X layer " + std::to_string(i)).c_str());
                vh1_x.push_back(h1_x);
                auto h1_y = new TH1D(("1D_y" + std::to_string(i)).c_str(), "", nbin, xlow, xup);
                h1_y->SetXTitle("Shift ay [#mum]");
                h1_y->SetTitle(("Shift Y layer " + std::to_string(i)).c_str());
                vh1_y.push_back(h1_y);
            }
            if (vangles.size() != Nlayer)
            {
                for (int l = 0; l < Nlayer; l++)
                {
                    vector<pair<double, double>> vpair;
                    vangles.push_back(vpair);
                    int hitview = 0;
                    for (int view = 0; view < NView; view++)
                    {
                        if (graph.GetLayer(view) != l) { continue; }
                        if (vangles[l].size() > 1000 * 100) { continue; }
                        hitview++;
                        for (int j = 0; j < NImager; j++)
                        {
                            string filepath = EachView.at(view * NImager + j).get <picojson::object>().at("TrackFilePath").get<string>();
                            vector<phst::MicroTrack> vmt;
                            read_bin(filepath, vmt);
                            for (auto p = vmt.begin(); p != vmt.end(); p++)
                            {
                                if (p->ph < phcut) { continue; }
                                vh2_2[l]->Fill(p->ax * 1000, p->ay * 1000);
                                vangles[l].push_back(make_pair(p->ax, p->ay));
                                vh1_x[l]->Fill(p->ax * 1000);
                                vh1_y[l]->Fill(p->ay * 1000);
                            }
                        }
                    }
                    vangleviews.emplace_back(hitview);
                }
            }
            else
            {
                for (int l = 0; l < Nlayer; l++)
                {
                    for (auto p = vangles[l].begin(); p != vangles[l].end(); p++)
                    {
                        vh2_2[l]->Fill(p->first * 1000, p->second * 1000);
                        vh1_x[l]->Fill(p->first * 1000);
                        vh1_y[l]->Fill(p->second * 1000);
                    }
                }
            }
            can->Clear();
            can->Divide(2, 3);
            for (int l = 0; l < Nlayer; l++)
            {
                vh2_2[l]->SetTitle(("Shift layer " + std::to_string(l) + " Nview " + std::to_string(vangleviews.at(l)) + (phcut == 0 ? "" : " Phcut " + std::to_string(phcut))).c_str());
            }
            for (int l = 0; l < Nlayer; l++)
            {
                can->cd(l + 1);
                vh2_2[l]->SetStats(0);
                vh2_2[l]->Draw("colz");
                can->cd(l + 3);
                vh1_x[l]->SetStats("emrou");
                vh1_x[l]->Draw();
                can->cd(l + 5);
                vh1_y[l]->SetStats("emrou");
                vh1_y[l]->Draw();
            }
            can->SaveAs(oname.c_str());
            for (int i = 0; i < vh2_2.size(); i++)
            {
                delete vh2_2[i];
                delete vh1_x[i];
                delete vh1_y[i];
            }
            vh2_2.clear();
            vh2_2.shrink_to_fit();
            vh1_x.clear();
            vh1_x.shrink_to_fit();
            vh1_y.clear();
            vh1_y.shrink_to_fit();
        }
        void makegraph_phvol(TCanvas* can, string oname, double phlow, double phup, double vollow, double volup, int nbin)
        {
            vector<TH2D*> vh2_2;
            vector<TH1D*> vh1_ph;
            vector<TH1D*> vh1_vol;
            for (int i = 0; i < Nlayer; i++)
            {
                auto h2 = new TH2D(("2D Ph Vol" + std::to_string(i)).c_str(), "", phup - phlow, phlow, phup, nbin, vollow, volup);
                h2->SetXTitle("Ph");
                h2->SetYTitle("Vol");
                h2->SetTitle(("Ph Vol layer " + std::to_string(i)).c_str());
                vh2_2.push_back(h2);
                auto h1_x = new TH1D(("1D_ph" + std::to_string(i)).c_str(), "", phup - phlow, phlow, phup);
                h1_x->SetXTitle("Ph");
                h1_x->SetTitle(("Ph layer " + std::to_string(i)).c_str());
                vh1_ph.push_back(h1_x);
                auto h1_y = new TH1D(("1D_vol" + std::to_string(i)).c_str(), "", nbin, vollow, volup);
                h1_y->SetXTitle("Vol");
                h1_y->SetTitle(("Vol layer " + std::to_string(i)).c_str());
                vh1_vol.push_back(h1_y);
            }
            if (vphvol.size() != Nlayer)
            {
                for (int l = 0; l < Nlayer; l++)
                {
                    vector<pair<double, double>> vpair;
                    vphvol.push_back(vpair);
                    int hitview = 0;
                    for (int view = 0; view < NView; view++)
                    {
                        if (graph.GetLayer(view) != l) { continue; }
                        if (vphvol[l].size() > 1000 * 100) { continue; }
                        hitview++;
                        for (int j = 0; j < NImager; j++)
                        {
                            string filepath = EachView.at(view * NImager + j).get <picojson::object>().at("TrackFilePath").get<string>();
                            vector<phst::MicroTrack> vmt;
                            read_bin(filepath, vmt);
                            for (auto p = vmt.begin(); p != vmt.end(); p++)
                            {
                                vh2_2[l]->Fill(p->ph, p->vol);
                                vphvol[l].push_back(make_pair(p->ph, p->vol));
                                vh1_ph[l]->Fill(p->ph);
                                vh1_vol[l]->Fill(p->vol);
                            }
                        }
                    }
                    vphvolviews.emplace_back(hitview);
                }
            }
            else
            {
                for (int l = 0; l < Nlayer; l++)
                {
                    for (auto p = vphvol[l].begin(); p != vphvol[l].end(); p++)
                    {
                        vh2_2[l]->Fill(p->first, p->second);
                        vh1_ph[l]->Fill(p->first);
                        vh1_vol[l]->Fill(p->second);
                    }
                }
            }
            can->Clear();
            can->Divide(2, 3);
            for (int l = 0; l < Nlayer; l++)
            {
                vh2_2[l]->SetTitle(("Ph Vol layer " + std::to_string(l) + " Nview " + std::to_string(vphvolviews.at(l))).c_str());
            }
            for (int l = 0; l < Nlayer; l++)
            {
                can->cd(l + 1);
                vh2_2[l]->SetStats(0);
                vh2_2[l]->Draw("colz");
                can->cd(l + 3);
                vh1_ph[l]->SetStats("emrou");
                vh1_ph[l]->Draw();
                can->cd(l + 5);
                vh1_vol[l]->SetStats("emrou");
                vh1_vol[l]->Draw();
            }
            can->SaveAs(oname.c_str());
            for (int i = 0; i < vh2_2.size(); i++)
            {
                delete vh2_2[i];
                delete vh1_ph[i];
                delete vh1_vol[i];
            }
            vh2_2.clear();
            vh2_2.shrink_to_fit();
            vh1_ph.clear();
            vh1_ph.shrink_to_fit();
            vh1_vol.clear();
            vh1_vol.shrink_to_fit();
        }
        void makegraph_angleph(TCanvas* can, string oname, double phlow, double phup, int nbin)
        {
            vector<TH2D*> vh2_2;
            vector<double> anglelow = { 0,0,0 };
            vector<double> angleup = { 25,100,250 };
            for (int a = 0; a < 3; a++) {
                for (int i = 0; i < Nlayer; i++)
                {
                    auto h2 = new TH2D(("Angle-Ph" + std::to_string(i) + "_" + std::to_string(a)).c_str(), "", nbin, anglelow[a], angleup[a], phup - phlow, phlow, phup);
                    h2->SetXTitle("Angle #sqrt{ax^2+ay^2} [#mum]");
                    h2->SetYTitle("Ph");
                    vh2_2.push_back(h2);
                }
            }
            vector<int> UsedNView;
            UsedNView.resize(Nlayer);
            for (int l = 0; l < Nlayer; l++)
            {
                vector<pair<double, double>> vpair;
                for (int view = 0; view < NView; view++)
                {
                    if (graph.GetLayer(view) != l) { continue; }
                    if (vh2_2[l]->GetEntries() > 1000 * 100) { continue; }
                    UsedNView[l]++;
                    for (int j = 0; j < NImager; j++)
                    {
                        string filepath = EachView.at(view * NImager + j).get <picojson::object>().at("TrackFilePath").get<string>();
                        vector<phst::MicroTrack> vmt;
                        read_bin(filepath, vmt);
                        for (auto p = vmt.begin(); p != vmt.end(); p++)
                        {
                            vh2_2[l]->Fill(std::hypot(p->ax, p->ay) * 1000, p->ph);
                            vh2_2[l + 2]->Fill(std::hypot(p->ax, p->ay) * 1000, p->ph);
                            vh2_2[l + 4]->Fill(std::hypot(p->ax, p->ay) * 1000, p->ph);
                        }
                    }
                }
            }
            can->Clear();
            can->Divide(2, 3);
            for (int l = 0; l < Nlayer; l++)
            {
                vh2_2[l]->SetTitle(("Angle ph layer " + std::to_string(l) + " Nview " + std::to_string(UsedNView.at(l))).c_str());
            }
            for (int a = 0; a < 3; a++) {
                for (int l = 0; l < Nlayer; l++)
                {
                    can->cd(l + 1 + a * 2);;
                    vh2_2[l + a * 2]->SetStats(0);
                    gPad->SetLogz();
                    vh2_2[l + a * 2]->Draw("colz");
                }
            }
            can->SaveAs(oname.c_str());
            gPad->SetLogz(0);
            for (int i = 0; i < vh2_2.size(); i++)
            {
                delete vh2_2[i];
            }
            vh2_2.clear();
            vh2_2.shrink_to_fit();
        }
        void makegraph_anglevol(TCanvas* can, string oname, double vollow, double volup, int nbin)
        {
            vector<TH2D*> vh2_2;
            vector<double> anglelow = { 0,0,0 };
            vector<double> angleup = { 25,100,250 };
            for (int a = 0; a < 3; a++) {
                for (int i = 0; i < Nlayer; i++)
                {
                    auto h2 = new TH2D(("Angle-Vol" + std::to_string(i) + "_" + std::to_string(a)).c_str(), "", nbin, anglelow[a], angleup[a], 50, vollow, volup);
                    h2->SetXTitle("Angle #sqrt{ax^2+ay^2} [#mum]");
                    h2->SetYTitle("Vol");
                    vh2_2.push_back(h2);
                }
            }
            vector<int> UsedNView;
            UsedNView.resize(Nlayer);
            for (int l = 0; l < Nlayer; l++)
            {
                vector<pair<double, double>> vpair;
                for (int view = 0; view < NView; view++)
                {
                    if (graph.GetLayer(view) != l) { continue; }
                    if (vh2_2[l]->GetEntries() > 1000 * 100) { continue; }
                    UsedNView[l]++;
                    for (int j = 0; j < NImager; j++)
                    {
                        string filepath = EachView.at(view * NImager + j).get <picojson::object>().at("TrackFilePath").get<string>();
                        vector<phst::MicroTrack> vmt;
                        read_bin(filepath, vmt);
                        for (auto p = vmt.begin(); p != vmt.end(); p++)
                        {
                            vh2_2[l]->Fill(std::hypot(p->ax, p->ay) * 1000, p->vol);
                            vh2_2[l + 2]->Fill(std::hypot(p->ax, p->ay) * 1000, p->vol);
                            vh2_2[l + 4]->Fill(std::hypot(p->ax, p->ay) * 1000, p->vol);
                        }
                    }
                }
            }
            can->Clear();
            can->Divide(2, 3);
            for (int l = 0; l < Nlayer; l++)
            {
                vh2_2[l]->SetTitle(("Angle vol layer " + std::to_string(l) + " Nview " + std::to_string(UsedNView.at(l))).c_str());
            }
            for (int a = 0; a < 3; a++) {
                for (int l = 0; l < Nlayer; l++)
                {
                    can->cd(l + 1 + a * 2);;
                    vh2_2[l + a * 2]->SetStats(0);
                    gPad->SetLogz();
                    vh2_2[l + a * 2]->Draw("colz");
                }
            }
            can->SaveAs(oname.c_str());
            gPad->SetLogz(0);
            for (int i = 0; i < vh2_2.size(); i++)
            {
                delete vh2_2[i];
            }
            vh2_2.clear();
            vh2_2.shrink_to_fit();
        }
    private:
        void bottom_top(vector<TH1D*>& vh1, int i) {
            gStyle->SetOptStat("nemr");
            vector<int> vcut{ 5,17 };
            vector<double> vpos{ 0.15,0.35 };
            TH1D* h1buf0 = new TH1D((string(vh1[i]->GetName()) + "buf" + to_string(vcut[0])).c_str(), "", vh1[i]->GetXaxis()->GetNbins(), vh1[i]->GetXaxis()->GetXmin(), vh1[i]->GetXaxis()->GetXmax());
            TH1D* h1buf1 = new TH1D((string(vh1[i]->GetName()) + "buf" + to_string(vcut[1])).c_str(), "", vh1[i]->GetXaxis()->GetNbins(), vh1[i]->GetXaxis()->GetXmin(), vh1[i]->GetXaxis()->GetXmax());
            double sumbuf = 0;
            double sumbuf0 = 0;
            double sumbuf1 = 0;
            ofstream ofs(ofolder + "Bottom-Top_Layer" + to_string(i) + ".txt");
            for (int j = 1; j <= vh1[i]->GetXaxis()->GetNbins(); j++)
            {
                ofs << j - 1 << " " << vh1[i]->GetBinContent(j) << endl;
                sumbuf += vh1[i]->GetBinContent(j);
            }
            for (int j = 1; j <= vcut[0] + 1; j++)
            {
                h1buf0->SetBinContent(j, vh1[i]->GetBinContent(j));
                sumbuf0 += vh1[i]->GetBinContent(j);
            }
            for (int j = vcut[1] + 1; j <= vh1[i]->GetXaxis()->GetNbins(); j++)
            {
                h1buf1->SetBinContent(j, vh1[i]->GetBinContent(j));
                sumbuf1 += vh1[i]->GetBinContent(j);
            }
            h1buf0->SetEntries(sumbuf0);
            h1buf0->SetStats(true);
            h1buf0->SetLineColor(2);
            h1buf0->SetLineStyle(2);
            {
                stringstream ss;
                ss << setprecision(3) << sumbuf0 * 100.0 / sumbuf;
                h1buf0->SetName(("N<=" + to_string(vcut[0]) + " " + ss.str() + "%").c_str());
            }
            h1buf0->Draw();
            gPad->Update();//IMPORTANT
            h1buf1->SetEntries(sumbuf1);
            h1buf1->SetStats(true);
            h1buf1->SetLineColor(4);
            h1buf1->SetLineStyle(4);
            {
                stringstream ss;
                ss << setprecision(3) << sumbuf1 * 100.0 / sumbuf;
                h1buf1->SetName(("N>=" + to_string(vcut[1]) + " " + ss.str() + "%").c_str());
            }
            h1buf1->Draw();
            gPad->Update();//IMPORTANT
            TPaveStats* stats0 = (TPaveStats*)h1buf0->FindObject("stats");
            TPaveStats* stats1 = (TPaveStats*)h1buf1->FindObject("stats");
            double x1 = stats0->GetX1NDC();
            stats0->SetX1NDC(vpos[0]);
            stats0->SetX2NDC(stats0->GetX2NDC() - x1 + vpos[0]);
            stats0->SetTextColor(2);
            stats1->SetX1NDC(vpos[1]);
            stats1->SetX2NDC(stats1->GetX2NDC() - x1 + vpos[1]);
            stats1->SetTextColor(4);
            vh1[i]->Draw();
            h1buf0->Draw("same");
            stats0->Draw("same");
            h1buf1->Draw("same");
            stats1->Draw("same");
            //http://heplife.blogspot.jp/2012/06/multiple-statistics-boxes-in-one-canvas.html
            vh1.push_back(h1buf0);
            vh1.push_back(h1buf1);
            gStyle->SetOptStat("emrou");
        }
        void make6graph(TCanvas* can, string oname, vector<TH2D*>& vh2, vector<TH1D*>& vh1, const vector<vector<double >>& vsum, int nbin,
            double vlow[2], double vup[2], double vave[2], string path1, string path2, string path3, bool textflag)
        {
            can->Clear();
            can->Divide(2, 3);
            vector<double> vNImager(NImager);
            vector<double> vNView(NView / 2);
            vector<TGraph*> vg1;
            vector<TH1D*> vaxis1;
            for (int i = 0; i < NImager; i++)
                vNImager[i] = i;
            for (int i = 0; i < NView / 2; i++)
                vNView[i] = i;

            gStyle->SetNumberContours(nbin);

            for (int i = 0; i < 2; i++)
            {
                can->cd(1 + i);
                vh2[i]->SetStats(false);
                vh2[i]->SetMinimum(vlow[i]);
                vh2[i]->SetMaximum(vup[i]);
                vh2[i]->Draw("colz");
                if (textflag) { vh2[i]->Draw("TEXT same"); }
                can->cd(3 + i);
                vh1[i]->Draw();

                can->cd(5 + i);
                TGraph* g1 = new TGraph();
                TH1D* axis1 = new TH1D();
                if (path3 == "views")
                {
                    g1 = new TGraph(NView / 2, vNView.data(), vsum[i].data());
                    axis1 = new TH1D(("ax" + std::to_string(i)).c_str(), "", NView / 2, 0, NView / 2 - 1);
                    axis1->SetXTitle("View ID");
                }
                else if (path3 == "sensors")
                {
                    can->cd(5 + i)->SetGrid();
                    g1 = new TGraph(NImager, vNImager.data(), vsum[i].data());
                    axis1 = new TH1D(("ax" + std::to_string(i)).c_str(), "", NImager, 0, NImager - 1);
                    axis1->SetXTitle("Imager ID");
                }
                else { throw std::exception(); }

                axis1->SetYTitle(GetAxisTitle(path1, path2).c_str());
                axis1->SetMinimum(vlow[i] + vave[i]);
                axis1->SetMaximum(vup[i] + vave[i]);
                axis1->SetStats(false);
                axis1->Draw();
                g1->Draw("PL");
                vg1.push_back(g1);
                vaxis1.push_back(axis1);
            }
            can->SaveAs(oname.c_str());
            for (int i = 0; i < vg1.size(); i++)
            {
                delete vg1[i];
            }
            vg1.clear();
            vg1.shrink_to_fit();
            for (int i = 0; i < vaxis1.size(); i++)
            {
                delete vaxis1[i];
            }
            vaxis1.clear();
            vaxis1.shrink_to_fit();
        }
        void make4graph(TCanvas* can, string oname, vector<TH2D*>& vh2, vector<TH1D*>& vh1, int nbin, double vlow[2], double vup[2], string path1, string path2)
        {
            can->Clear();
            can->Divide(2, 3);
            vector<double> vNImager(NImager);
            for (int i = 0; i < NImager; i++)
                vNImager[i] = i;

            gStyle->SetNumberContours(nbin);

            for (int i = 0; i < 2; i++)
            {
                can->cd(1 + i);
                vh2[i]->SetStats(false);
                vh2[i]->SetMinimum(vlow[i]);
                vh2[i]->SetMaximum(vup[i]);
                vh2[i]->Draw("colz");
                can->cd(3 + i);
                vh1[i]->Draw();
                if (path1 == "Bottom-Top")
                {
                    bottom_top(vh1, i);
                }
            }
            can->SaveAs(oname.c_str());
        }
        void make3p2graph(TCanvas* can, string oname, vector<TH2D*>& vh2_1, vector<TH2D*>& vh2_2, TH1D*& h1, TH2D*& h2)
        {
            can->Clear();
            can->Divide(2, 3);
            gStyle->SetPaintTextFormat("2.0f");
            for (int i = 0; i < Nlayer; i++)
            {
                can->cd(1 + i);
                vh2_1.at(i)->SetStats(0);
                vh2_1.at(i)->Draw("COLZ");
                //vh2_1.at(i)->Draw("TEXT same");
            }
            can->cd(3);
            h1->Draw();

            can->cd(4);
            h2->SetStats(0);
            h2->Draw("COLZ");


            can->SaveAs(oname.c_str());
            can->Clear();
            can->Divide(2, 3);

            gStyle->SetPaintTextFormat("3.0f");
            for (int i = 0; i < Nlayer; i++)
            {
                can->cd(1 + i);
                vh2_2.at(i)->SetStats(0);
                vh2_2.at(i)->Draw("COLZ");
                vh2_2.at(i)->Draw("TEXT same");
            }
            double x, y;
            double r = 5;
            vector<TH1D> anglehist;
            anglehist.push_back(TH1D("axl0", "ax Layer 0", 50, -10, 10));
            anglehist.push_back(TH1D("ayl0", "ay Layer 0", 50, -10, 10));
            anglehist.push_back(TH1D("axl1", "ax Layer 1", 50, -10, 10));
            anglehist.push_back(TH1D("ayl1", "ay Layer 1", 50, -10, 10));
            for (int i = 0; i < Nlayer; i++)
            {
                can->cd(3 + i);
                vh2_2.at(i)->SetStats(0);
                vh2_2.at(i)->Draw("COL");
                for (int j = 0; j < NView; j++)
                {
                    if (graph.GetLayer(j) != i) { continue; }
                    double stage_x = graph.GetStage(j, 0);
                    double stage_y = graph.GetStage(j, 1);
                    graph.GetBinDiff(j, vh2_2, x, y);
                    if (graph.GetViewFlag(j)) {
                        anglehist[i * 2 + 0].Fill(x);
                        anglehist[i * 2 + 1].Fill(y);
                    }
                    TArrow* arrow = new TArrow(stage_x, stage_y, stage_x + x * r, stage_y + y * r, 0.003, ">");
                    arrow->SetFillColor(1);
                    arrow->SetLineColor(1);
                    arrow->SetFillStyle(1001);
                    arrow->SetLineWidth(1);
                    arrow->SetLineStyle(1);
                    arrow->Draw();
                }
                refference_arrow(r, "1 mrad", graph.GetMin_sx(), graph.GetMin_sy());
            }
            can->SaveAs(oname.c_str());
            can->Clear();
            can->Divide(2, 3);
            can->cd(0 + 1);
            anglehist[0].Draw();
            can->cd(2 + 1);
            anglehist[1].Draw();
            can->cd(1 + 1);
            anglehist[2].Draw();
            can->cd(3 + 1);
            anglehist[3].Draw();
            can->SaveAs(oname.c_str());
        }
        void makep2graph(TCanvas* can, string oname, vector<TH2D*>& vh2, TH1D* h1, TH2D* h2)
        {
            can->Clear();
            can->Divide(2, 3);
            for (int i = 0; i < Nlayer; i++)
            {
                can->cd(1 + i);
                vh2.at(i)->SetStats(0);
                vh2.at(i)->Draw("COLZ");
            }
            can->cd(3);
            h1->Draw();

            can->cd(4);
            h2->SetStats(0);
            h2->Draw("COLZ");
            can->SaveAs(oname.c_str());
        }
        void getarray(vector<double>& arr, const int view, string path1, string path2)
        {
            auto history = &ViewHistory.at(view);
            auto h = history->get<picojson::object>();
            arr.reserve(NImager);
            int layer = graph.GetLayer(view);
            if (path1 == "Nogs" && path2 == "")
            {
                picojson::array sapn = h.at("StartAnalysisPicNo").get<picojson::array>();
                picojson::array nogs = h.at(path1.c_str()).get<picojson::array>();

                for (int j = 0; j < NImager; j++)
                {
                    double value = 0;
                    for (int k = int(sapn.at(j).get<double>()); k<int(sapn.at(j).get<double>()) + 16; k++)
                    {
                        value += nogs.at(j).get<picojson::array>().at(k).get<double>();
                    }
                    value /= 16;
                    arr.push_back(value);
                }
            }
            else if (path1 == "Nogs" && path2 != "")
            {
                picojson::array sapn = h.at("StartAnalysisPicNo").get<picojson::array>();
                picojson::array nogs = h.at(path1.c_str()).get<picojson::array>();

                for (int j = 0; j < NImager; j++)
                {
                    double value = 0;
                    value = nogs.at(j).get<picojson::array>().at(std::stoi(path2) + int(sapn.at(j).get<double>())).get<double>();
                    arr.push_back(value);
                }
            }
            else if (path1 == "Bottom-Top")
            {
                picojson::array detail = h.at("SurfaceDetail").get<picojson::array>();
                for (int j = 0; j < NImager; j++)
                {
                    double bottom = detail.at(j).get<picojson::object>().at("Bottom").get<double>();
                    double top = detail.at(j).get<picojson::object>().at("Top").get<double>();
                    arr.push_back(bottom - top);
                }
            }
            else if (path1 == "Bottom" || path1 == "Top")
            {
                picojson::array detail = h.at("SurfaceDetail").get<picojson::array>();
                for (int j = 0; j < NImager; j++)
                {
                    arr.push_back(detail.at(j).get<picojson::object>().at(path1).get<double>());
                }
            }
            else if (path1 == "FineZ" || path1 == "FineZ W/DZ")
            {
                double stage_z = h.at("ScanLines").get<picojson::object>().at("Z").get<double>();
                double thick = h.at("ScanEachLayerParam").get<picojson::object>().at("ThickOfLayer").get<double>();
                double NPicThickOfLayer = 15;
                if (h.at("ScanEachLayerParam").get<picojson::object>().find("NPicThickOfLayer") != h.at("ScanEachLayerParam").get<picojson::object>().end())
                {
                    NPicThickOfLayer = h.at("ScanEachLayerParam").get<picojson::object>().at("NPicThickOfLayer").get<double>();
                }
                picojson::array sapn = h.at("StartAnalysisPicNo").get<picojson::array>();
                for (int j = 0; j < NImager; j++)
                {
                    double value = stage_z * 1000.0 + thick / NPicThickOfLayer * sapn.at(j).get<double>();
                    if (path1 == "FineZ W/DZ") {
                        value += Affine[j].get<picojson::object>().at("DZ").get<double>() * 1000;
                    }
                    //layer==0の時は解析に使った最初の画像を1枚目としたときの17枚目の画像の座標
                    //layer==1の時は解析に使った最初の画像の座標
                    if (layer == 0) { value += (thick / 15 * 16); }
                    value *= -1;
                    arr.push_back(value);
                }
            }
            else if (path1 == "VeryFineZ")
            {
                auto h = history->get<picojson::object>();
                double thick = h.at("ScanEachLayerParam").get<picojson::object>().at("ThickOfLayer").get<double>();
                double NPicThickOfLayer = 15;
                if (h.at("ScanEachLayerParam").get<picojson::object>().find("NPicThickOfLayer") != h.at("ScanEachLayerParam").get<picojson::object>().end())
                {
                    NPicThickOfLayer = h.at("ScanEachLayerParam").get<picojson::object>().at("NPicThickOfLayer").get<double>();
                }
                double stage_z = h.at("ScanLines").get<picojson::object>().at("Z").get<double>();
                picojson::array sapn = h.at("StartAnalysisPicNo").get<picojson::array>();
                picojson::array nogs = h.at("Nogs").get<picojson::array>();
                picojson::array detail = h.at("SurfaceDetail").get<picojson::array>();
                vector<vector<double>> vvnogs;

                for (int j = 0; j < NImager; j++)
                {
                    vector<double> vnogs;
                    for (int k = 0; k < nogs.at(j).get<picojson::array>().size(); k++)
                    {
                        vnogs.push_back(nogs.at(j).get<picojson::array>().at(k).get<double>());
                    }
                    vvnogs.push_back(vnogs);
                }
                for (int j = 0; j < NImager; j++)
                {
                    int top = detail.at(j).get<picojson::object>().at("Top").get<double>();
                    int bottom = detail.at(j).get<picojson::object>().at("Bottom").get<double>();
                    double SumNog = 0;
                    for (int k = top; k < bottom; k++)
                    {
                        SumNog += vvnogs.at(j).at(k);
                    }
                    SumNog /= double(bottom - top);
                    double HalfNog = SumNog / 2;
                    double picnum = 0;

                    if (layer == 0)
                    {
                        if (vvnogs.at(j).back() > HalfNog) { picnum = vvnogs.at(j).size() - 1; }
                        else
                        {
                            for (int k = vvnogs.at(j).size() - 2; k >= 0; k--)
                            {
                                if (vvnogs.at(j).at(k) > HalfNog)
                                {
                                    picnum = ((vvnogs.at(j).at(k) - HalfNog) * (k + 1) + (HalfNog - vvnogs.at(j)[k + 1]) * k) / (vvnogs.at(j).at(k) - vvnogs.at(j)[k + 1]);
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (vvnogs.at(j).front() > HalfNog) { picnum = 0; }
                        else
                        {
                            for (int k = 1; k < vvnogs.at(j).size(); k++)
                            {
                                if (vvnogs.at(j).at(k) > HalfNog)
                                {
                                    picnum = ((vvnogs.at(j).at(k) - HalfNog) * (k - 1) + (HalfNog - vvnogs.at(j)[k - 1]) * k) / (vvnogs.at(j).at(k) - vvnogs.at(j)[k - 1]);
                                    break;
                                }
                            }
                        }
                    }
                    double value = stage_z * 1000.0 + thick / NPicThickOfLayer * picnum;
                    value *= -1;
                    arr.push_back(value);
                }
            }
            else if (path1 == "# of tracks")
            {
                arr.resize(NImager, 0);
                if (vnumberoftracks.size() > view)
                {
                    arr = vnumberoftracks.at(view);
                }
                else
                {

                    vector<double> numberoftracks(NImager, 0);
#pragma omp parallel for
                    for (int j = 0; j < NImager; j++)
                    {
                        string filepath = EachView.at(view * NImager + j).get <picojson::object>().at("TrackFilePath").get<string>();
                        numberoftracks[j] = arr[j] = double(get_filesize(filepath) / 32);
                    }
                    vnumberoftracks.push_back(numberoftracks);
                }
            }
            else if (path1 == "# of unclustered tracks")
            {
                arr.resize(NImager, 0);
                if (vnumberoftracks_unclustered.size() > view)
                {
                    arr = vnumberoftracks_unclustered.at(view);
                }
            }
            else if (path1 == "MainProcessTime")
            {
                arr.resize(NImager, 0);
                if (velapsed.size() > view)
                {
                    arr = velapsed.at(view);
                }
            }
            else if (path2 != "")
            {
                auto arrbuf = h.at(path1).get<picojson::object>().at(path2).get<picojson::array>();
                for (int i = 0; i < arrbuf.size(); i++)
                {
                    arr.push_back(arrbuf[i].get<double>());
                }
            }
            else
            {
                auto arrbuf = h.at(path1).get<picojson::array>();
                for (int i = 0; i < arrbuf.size(); i++)
                {
                    arr.push_back(arrbuf[i].get<double>());
                }
            }
        }
        void getvalue_process(double& value, const int view, int processno)
        {
            auto history = &ViewHistory.at(view);
            auto h = history->get<picojson::object>();
            auto p = h.at("ProcessTimeMain").get<picojson::object>();
            int repeatno = 0;
            if (h.size() <= 1) { value = -1; return; }
            while (true)
            {
                string str1 = "Process" + std::to_string(processno) + " " + std::to_string(repeatno);
                if (p.find(str1) != p.end())
                {
                    if (processno == 0)
                    {
                        value = p.at(str1).get<double>();
                    }
                    else
                    {
                        string str2 = "Process" + std::to_string(processno - 1) + " " + std::to_string(repeatno);
                        value = p.at(str1).get<double>() - p.at(str2).get<double>();
                    }
                    break;
                }
                repeatno++;
                if (repeatno > 10) { value = -1; return; }
            }
            value *= 1000;
        }
        void getvalue(double& value, const int view, string path1, string path2)
        {
            auto history = &ViewHistory.at(view);
            auto h = history->get<picojson::object>();
            TString Tpath1 = path1;
            TObjArray* tx = Tpath1.Tokenize(" ");
            if (path1 == "Positions0" || path1 == "Positions1")
            {
                double target = h.at("ScanLines").get<picojson::object>().at(path2).get<double>();
                double real = h.at(path1).get<picojson::object>().at(path2).get<double>();
                value = real - target;
                value *= 1000;
            }
            else if (path1 == "ScanLinesOrigin")
            {
                value = ScanLinesOrigin.at(view).get<picojson::object>().at(path2).get<double>();
            }
            else if (path1 == "Z1-Z0")
            {
                value = h.at("Z1").get<double>() - h.at("Z0").get<double>();
            }
            else if (path1 == "MaxProcessTimeImageFilter1")
            {
                auto arrbuf = h.at("ProcessTimeImageFilter1").get<picojson::array>();
                vector<double> vecbuf(arrbuf.size()); for (int i = 0; i < arrbuf.size(); i++) { vecbuf[i] = arrbuf[i].get<double>(); }
                value = *max_element(vecbuf.begin(), vecbuf.end()); value *= 1000.0;
            }
            else if (path1 == "ImagerID of MaxProcessTimeImageFilter1")
            {
                auto arrbuf = h.at("ProcessTimeImageFilter1").get<picojson::array>();
                vector<double> vecbuf(arrbuf.size()); for (int i = 0; i < arrbuf.size(); i++) { vecbuf[i] = arrbuf[i].get<double>(); }
                value = max_element(vecbuf.begin(), vecbuf.end()) - vecbuf.begin();
            }
            else if (path1 == "ImagerID of MinProcessTimeImageFilter1")
            {
                auto arrbuf = h.at("ProcessTimeImageFilter1").get<picojson::array>();
                vector<double> vecbuf(arrbuf.size()); for (int i = 0; i < arrbuf.size(); i++) { vecbuf[i] = arrbuf[i].get<double>(); }
                value = min_element(vecbuf.begin(), vecbuf.end()) - vecbuf.begin();
            }
            else if (path1 == "ProcessTimeMain")
            {
                if (view < ViewHistory.size() - 1) {
                    value = ViewHistory
                        .at(view + 1).get<picojson::object>()
                        .at(path1).get<picojson::object>()
                        .at(path2).get<double>() - h.at(path1).get<picojson::object>().at(path2).get<double>();
                }
                else
                {
                    value = 0;
                }
            }
            else if (path1 == "DrivingTimeXYZ")
            {
                int AX = std::stoi(path2);
                value = h.at(path1).get<picojson::array>().at(AX).get<double>();
            }
            else if (path1 == "DrivingTimePiezo")
            {
                value = h.at(path1).get<double>();
            }
            else if (path1 == "DrivingTime")
            {
                value = h.at(path1).get<double>();
            }
            else if (path1 == "ScanEachLayerParam" && path2 == "FineThickOfLayer")
            {
                auto found = h.at("SurfaceSummary").get<picojson::object>().at("Found").get<picojson::array>();
                auto thick = h.at("SurfaceSummary").get<picojson::object>().at("Thick").get<picojson::array>();
                auto thickness = h.at("ScanEachLayerParam").get<picojson::object>().at("ThickOfLayer").get<double>();
                double Npicthickoflayer = 15;
                if (h.at("ScanEachLayerParam").get<picojson::object>().find("NPicThickOfLayer") != h.at("ScanEachLayerParam").get<picojson::object>().end())
                {
                    Npicthickoflayer = h.at("ScanEachLayerParam").get<picojson::object>().at("NPicThickOfLayer").get<double>();
                }
                int j = 0;
                for (int i = 0; i < found.size(); i++)
                {
                    if (found.at(i).get<bool>())
                    {
                        j++;
                        value += thick.at(i).get<double>();
                    }
                }
                if (j > 0)
                {
                    value /= j;
                    value = thickness * (value - 3.0) / Npicthickoflayer;
                }
                else
                {
                    value = -1;
                }
            }
            else if (path1 == "StartAnalysisPicNo" && path2 == "Max-Min+16")
            {
                picojson::array sapn = h.at(path1).get<picojson::array>();
                double MinNo = sapn.at(0).get<double>();
                double MaxNo = sapn.at(0).get<double>();
                for (int j = 1; j < NImager; j++)
                {
                    double Val = sapn.at(j).get<double>();
                    if (MinNo > Val)
                    {
                        MinNo = Val;
                    }
                    if (MaxNo < Val)
                    {
                        MaxNo = Val;
                    }
                }
                value = MaxNo - MinNo + 16;
            }
            else if (((TObjString*)(tx->At(0)))->String()(0, 7) == "Damping")
            {
                int Fpic = 0;
                int Npic = INT32_MAX;
                if (tx->GetEntries() == 2)
                {
                    Npic = stoi((std::string)((TObjString*)(tx->At(1)))->String());
                }
                else if (tx->GetEntries() == 3)
                {
                    Fpic = stoi((std::string)((TObjString*)(tx->At(1)))->String());
                    Npic = stoi((std::string)((TObjString*)(tx->At(2)))->String());
                }
                value = 0;
                vector<double> vvalue;
                if (Results2.at(view).is<picojson::array>()) {
                    auto arr1 = Results2.at(view).get<picojson::array>();
                    //センサ数
                    for (int i = 0; i < arr1.size() && i < 1; i++) {
                        auto arr2 = arr1[i].get<picojson::array>();
                        for (int j = Fpic; j < arr2.size() && j < Npic; j++) {
                            auto obj = arr2[j].get<picojson::object>();
                            if (obj.at("sum").get<double>() == 0)continue;
                            vvalue.push_back(obj.at(path2).get<double>());
                        }
                    }
                }
                else { value = std::numeric_limits<double>::min(); }
                if (((TObjString*)(tx->At(0)))->String() == "DampingRMS")
                {
                    if (vvalue.size() == 0) { value = std::numeric_limits<double>::min(); }
                    else { value = rms(vvalue) * 1000; }
                }
                else if (((TObjString*)(tx->At(0)))->String() == "DampingMax")
                {
                    if (vvalue.size() == 0) { value = std::numeric_limits<double>::min(); }
                    else { value = absmax(vvalue) * 1000; }
                }
                else if (((TObjString*)(tx->At(0)))->String() == "Damping*15")
                {
                    if (vvalue.size() == 0) { value = std::numeric_limits<double>::min(); }
                    else { value = std::accumulate(vvalue.begin(), vvalue.end(), 0.0) / vvalue.size() * 1000 * 15; }
                }
                else if (((TObjString*)(tx->At(0)))->String() == "DampingAve")
                {
                    if (vvalue.size() == 0) { value = std::numeric_limits<double>::min(); }
                    else { value = std::accumulate(vvalue.begin(), vvalue.end(), 0.0) / vvalue.size(); }
                }
                else if (((TObjString*)(tx->At(0)))->String() == "DampingSum")
                {
                    if (vvalue.size() == 0) { value = std::numeric_limits<double>::min(); }
                    else { value = std::accumulate(vvalue.begin(), vvalue.end(), 0.0) * 1000; }
                }
            }
            else if (path2 != "")
            {
                if (h.at(path1).get<picojson::object>().at(path2).is<double>())
                {
                    value = h.at(path1).get<picojson::object>().at(path2).get<double>();
                }
                else if (h.at(path1).get<picojson::object>().at(path2).is<bool>())
                {
                    value = h.at(path1).get<picojson::object>().at(path2).get<bool>() ? 1 : 0;
                }
                else
                {
                    throw std::exception();
                }
            }
            else
            {
                if (h.at(path1).is<double>())
                {
                    value = h.at(path1).get<double>();
                }
                else
                {
                    throw std::exception();
                }
            }
        }
        void getbin(string path1, string path2, int& nbin, double& xlow, double& xup)
        {
            if (path1 == "StartAnalysisPicNo" && path2 == "Max-Min+16")
            {
                xup = ViewHistory.at(0).get<picojson::object>().at("Nogs").get<picojson::array>().at(0).get<picojson::array>().size();
                xlow = 16;
                nbin = (xup - xlow) + 1;
                xup += 0.5; xlow -= 0.5;
            }
            else if (path1 == "StartAnalysisPicNo")
            {
                xup = ViewHistory.at(0).get<picojson::object>().at("Nogs").get<picojson::array>().at(0).get<picojson::array>().size() - 16;
                xlow = 0;
                nbin = xup + 1;
                xup += 0.5; xlow -= 0.5;
            }
            else if (path1 == "Bottom-Top" || path1 == "Bottom" || path1 == "Top" || path2 == "AveOfThickFound")
            {
                xup = ViewHistory.at(0).get<picojson::object>().at("Nogs").get<picojson::array>().at(0).get<picojson::array>().size();
                xlow = 0;
                nbin = xup + 1;
                xup += 0.5; xlow -= 0.5;
            }
            else if (path1 == "SurfaceSummary" && (path2 == "Found" || path2 == "Nup" || path2 == "Ndown"))
            {
                xup = ViewHistory.at(0).get<picojson::object>().at("Nogs").get<picojson::array>().size();
                xlow = 0;
                nbin = xup + 1;
                xup += 0.5; xlow -= 0.5;
            }
            else if (path1 == "SurfaceSummary" && (path2 == "AveOfCenter" || path2 == "AveOfCenterFound"))
            {
                xup = (ViewHistory.at(0).get<picojson::object>().at("Nogs").get<picojson::array>().at(0).get<picojson::array>().size() - 16) / 2;
                xlow = -xup;
                nbin = 50;
            }
        }
    public:
        void getsummary(string oname)
        {
            for (int l = 0; l < 2; l++)
            {
                summary.all_emul[l] = 0;
                summary.out_emul[l] = 0;
                summary.edge_up[l] = 0;
                summary.edge_down[l] = 0;
                summary.lack_pic[l] = 0;
                summary.dark_emul[l] = 0;
                summary.sum_nog[l] = 0;
                summary.sum_nog0[l] = 0;
                summary.sum_nog15[l] = 0;
                summary.sum_not[l] = 0;

            }
            for (int i = 0; i < ViewHistory.size(); i++)
            {
                auto h = ViewHistory.at(i).get<picojson::object>();
                int Layer = (int)h.at("ScanLines").get<picojson::object>().at("Layer").get<double>();
                auto surfacedetail = h.at("SurfaceDetail").get<picojson::array>();
                auto surfacesummary = h.at("SurfaceSummary").get<picojson::object>();
                auto nogs = h.at("Nogs").get<picojson::array>();
                auto startanalysispicno = h.at("StartAnalysisPicNo").get<picojson::array>();
                auto exposurecount = h.at("ImagerControllerParam").get<picojson::object>().at("ExposureCount").get<picojson::array>();
                summary.edge_up[Layer] += surfacesummary.at("Nup").get<double>();
                summary.edge_down[Layer] += surfacesummary.at("Ndown").get<double>();
                summary.lack_pic[Layer] += surfacesummary.at("Nlack").get<double>();
                for (int j = 0; j < NImager; j++)
                {
                    auto d = surfacedetail.at(j).get<picojson::object>();
                    if (d.at("Found").get<bool>())
                    {
                        int start = startanalysispicno.at(j).get<double>();
                        double sum = 0;
                        for (int k = start; k < start + 16; k++)
                        {
                            sum += nogs.at(j).get<picojson::array>().at(k).get<double>() * (1.0 / 16.0);
                        }
                        summary.sum_nog[Layer] += sum;
                        if (nogs.at(j).get<picojson::array>().at(0 + start).get<double>() < sum / 2)
                        {
                            summary.sum_nog0[Layer] += 1;
                        }
                        if (nogs.at(j).get<picojson::array>().at(15 + start).get<double>() < sum / 2)
                        {
                            summary.sum_nog15[Layer] += 1;
                        }
                        if (exposurecount.at(j).get<double>() == 999)
                        {
                            summary.dark_emul[Layer] += 1;
                        }
                        if (vnumberoftracks.size() > i)
                        {
                            summary.sum_not[Layer] += vnumberoftracks[i][j];
                        }
                    }
                    else
                    {
                        summary.out_emul[Layer] += 1;
                    }
                }
                summary.all_emul[Layer] += NImager;
            }
            summary.show();
            summary.save(oname);
        }
        double getavenot(int type)
        {
            vector<vector<double>> vvt;
            if (type == 1) {
                vvt = vnumberoftracks;
            }
            else if (type == 2) {
                vvt = vnumberoftracks_unclustered;
            }
            else { return -1; }
            double val = 0;
            int num = 0;
            for (int i = 0; i < vvt.size(); i++)
            {
                for (int j = 0; j < vvt[i].size(); j++)
                {
                    if (vvt[i][j] >= 10)
                    {
                        val += vvt[i][j];
                        num++;
                    }
                }
            }
            if (num == 0)
            {
                return -1;
            }
            else
            {
                return val / num;
            }
        }
        void makedamping1(TCanvas* can, string oname, picojson::array Results2)
        {
            this->Results2 = Results2;
            makegraph1(can, oname, "DampingAve", "sum", 0, 5000, 50, 4);

            makegraph1(can, oname, "DampingRMS 0 5", "xy_shift.x", 0, 0.5, 50, 4);
            makegraph1(can, oname, "DampingRMS 5 10", "xy_shift.x", 0, 0.5, 50, 4);
            makegraph1(can, oname, "DampingRMS 10 15", "xy_shift.x", 0, 0.5, 50, 4);
            makegraph1(can, oname, "DampingRMS 15 20", "xy_shift.x", 0, 0.5, 50, 4);
            makegraph1(can, oname, "DampingRMS 20 25", "xy_shift.x", 0, 0.5, 50, 4);
            makegraph1(can, oname, "DampingRMS 25 30", "xy_shift.x", 0, 0.5, 50, 4);
            makegraph1(can, oname, "DampingRMS 0 15", "xy_shift.x", 0, 0.5, 50, 4);
            makegraph1(can, oname, "DampingRMS", "xy_shift.x", 0, 0.5, 50, 4);
            makegraph1(can, oname, "Damping*15 5", "xy_shift.x", -5, 5, 50, 4);
            makegraph1(can, oname, "Damping*15 15", "xy_shift.x", -1, 1, 50, 4);
            makegraph1(can, oname, "Damping*15", "xy_shift.x", -1, 1, 50, 4);
            makegraph1(can, oname, "DampingMax", "xy_shift.x", 0, 2, 50, 4);

            makegraph1(can, oname, "DampingRMS 0 5", "xy_shift.y", 0, 0.5, 50, 4);
            makegraph1(can, oname, "DampingRMS 5 10", "xy_shift.y", 0, 0.5, 50, 4);
            makegraph1(can, oname, "DampingRMS 10 15", "xy_shift.y", 0, 0.5, 50, 4);
            makegraph1(can, oname, "DampingRMS 15 20", "xy_shift.y", 0, 0.5, 50, 4);
            makegraph1(can, oname, "DampingRMS 20 25", "xy_shift.y", 0, 0.5, 50, 4);
            makegraph1(can, oname, "DampingRMS 25 30", "xy_shift.y", 0, 0.5, 50, 4);
            makegraph1(can, oname, "DampingRMS 0 15", "xy_shift.y", 0, 0.5, 50, 4);
            makegraph1(can, oname, "DampingRMS", "xy_shift.y", 0, 0.5, 50, 4);
            makegraph1(can, oname, "Damping*15 5", "xy_shift.y", -5, 5, 50, 4);
            makegraph1(can, oname, "Damping*15 15", "xy_shift.y", -1, 1, 50, 4);
            makegraph1(can, oname, "Damping*15", "xy_shift.y", -1, 1, 50, 4);
            makegraph1(can, oname, "DampingMax", "xy_shift.y", 0, 2, 50, 4);
        }
        void makedamping2(TCanvas* can, string oname, picojson::array Results2)
        {
            this->Results2 = Results2;
            for (int j = 0; j < 30; j++)
            {
                makegraph1(can, oname, "DampingSum " + to_string(j) + " 30", "xy_shift.x", -1, 1, 50, 4);
            }
            for (int j = 0; j < 30; j++)
            {
                makegraph1(can, oname, "DampingSum " + to_string(j) + " 30", "xy_shift.y", -1, 1, 50, 4);
            }

            for (int j = 0; j < 29; j++)
            {
                makegraph1(can, oname, "DampingSum " + to_string(j) + " " + to_string(j + 1), "xy_shift.x", -1, 1, 50, 4);
            }
            for (int j = 0; j < 29; j++)
            {
                makegraph1(can, oname, "DampingSum " + to_string(j) + " " + to_string(j + 1), "xy_shift.y", -1, 1, 50, 4);
            }
        }
    };
    void SetBinContent(TH2D* h2, const vector<double>& vpx, const vector<double>& vpy, const picojson::array& AffineParam, const vector<double>& vval) {
        for (int i = 0; i < vval.size(); i++) {
            auto& Aff_coef = AffineParam[i].get<picojson::object>().at("Aff_coef").get<picojson::array>();

            h2->SetBinContent(
                std::find(vpx.begin(), vpx.end(), Graph::GetAlignedImagerView_X(0.0, Aff_coef[4].get<double>())) - vpx.begin() + 1,
                std::find(vpy.begin(), vpy.end(), Graph::GetAlignedImagerView_Y(0.0, Aff_coef[5].get<double>())) - vpy.begin() + 1, vval[i]);
        }
    }
    void MakeAffineGraph(picojson::array& AffineParam) {
        TCanvas* can = new TCanvas();
        gStyle->SetOptStat("emrou");
        can->Divide(2, 3);
        string folder = "";
        string oname = "affine.pdf";
        can->SaveAs((folder + oname + "[").c_str(), "Title:graph");

        vector<double> vpx, vpy;
        vector<double> vex, vey, ve;
        {
            vector<TH1D*> v_h1;
            v_h1.emplace_back(new TH1D("ex", "", 50, 0.45, 0.46));
            v_h1.emplace_back(new TH1D("ey", "", 50, 0.45, 0.46));
            v_h1.emplace_back(new TH1D("px", "", 50, -5, 5));
            v_h1.emplace_back(new TH1D("py", "", 50, -5, 5));
            v_h1[0]->SetTitle("ex");
            v_h1[1]->SetTitle("ey");
            v_h1[2]->SetTitle("px");
            v_h1[3]->SetTitle("py");
            for (auto& p : AffineParam) {
                auto& Aff_coef = p.get<picojson::object>().at("Aff_coef").get<picojson::array>();
                v_h1[0]->Fill(std::hypot(Aff_coef[0].get<double>(), Aff_coef[1].get<double>()) * 1000);
                v_h1[1]->Fill(std::hypot(Aff_coef[2].get<double>(), Aff_coef[3].get<double>()) * 1000);
                v_h1[2]->Fill(Aff_coef[4].get<double>());
                v_h1[3]->Fill(Aff_coef[5].get<double>());
                vex.emplace_back(std::hypot(Aff_coef[0].get<double>(), Aff_coef[1].get<double>()) * 1000);
                vey.emplace_back(std::hypot(Aff_coef[2].get<double>(), Aff_coef[3].get<double>()) * 1000);
                ve.emplace_back((vex.back() + vey.back()) / 2);
                vpx.emplace_back(Graph::GetAlignedImagerView_X(0.0, Aff_coef[4].get<double>()));
                vpy.emplace_back(Graph::GetAlignedImagerView_Y(0.0, Aff_coef[5].get<double>()));
            }
            for (int i = 0; i < 4; i++) {
                can->cd(i + 1);
                v_h1[i]->Draw();
            }
            can->SaveAs((folder + oname).c_str());

            for (auto& p : v_h1) { delete p; }
        }

        vector<double> vsx, vsy, vsz, vdz;
        {
            vector<TH1D*> v_h1;
            v_h1.emplace_back(new TH1D("sigma_x", "", 40, 0, 0.2));
            v_h1.emplace_back(new TH1D("sigma_y", "", 40, 0, 0.2));
            v_h1.emplace_back(new TH1D("sigma_z", "", 50, 0, 1));
            v_h1.emplace_back(new TH1D("dz", "", 50, -20, 20));
            v_h1[0]->SetTitle("sigma_x");
            v_h1[1]->SetTitle("sigma_y");
            v_h1[2]->SetTitle("sigma_z");
            v_h1[3]->SetTitle("dz");
            v_h1[0]->GetYaxis()->SetTitle("entries");
            v_h1[1]->GetYaxis()->SetTitle("entries");
            v_h1[2]->GetYaxis()->SetTitle("entries");
            v_h1[3]->GetYaxis()->SetTitle("entries");
            for (auto& p : AffineParam) {
                v_h1[0]->Fill(p.get<picojson::object>().at("sigma_x").get<double>() * 1000);
                v_h1[1]->Fill(p.get<picojson::object>().at("sigma_y").get<double>() * 1000);
                v_h1[2]->Fill(p.get<picojson::object>().at("sigma_z").get<double>() * 1000);
                v_h1[3]->Fill(p.get<picojson::object>().at("dz").get<double>() * 1000);
                vsx.emplace_back(p.get<picojson::object>().at("sigma_x").get<double>() * 1000);
                vsy.emplace_back(p.get<picojson::object>().at("sigma_y").get<double>() * 1000);
                vsz.emplace_back(p.get<picojson::object>().at("sigma_z").get<double>() * 1000);
                vdz.emplace_back(p.get<picojson::object>().at("dz").get<double>() * 1000);
            }
            gStyle->SetOptStat("emr");
            for (int i = 0; i < 4; i++) {
                can->cd(i + 1);
                v_h1[i]->GetYaxis()->SetTitle("entries");
                v_h1[i]->GetXaxis()->SetTitle("[#mum]");
                v_h1[i]->Draw();
            }
            can->SaveAs((folder + oname).c_str());
            can->Clear();
            v_h1[0]->Draw();
            can->SaveAs("sigma_x.root");
            can->Clear();
            v_h1[1]->Draw();
            can->SaveAs("sigma_y.root");
            can->SaveAs((folder + oname).c_str());

            for (auto& p : v_h1) { delete p; }
        }

        vector<double> va1, va2, vhit, vsig;

        {
            vector<TH1D*> v_h1;
            v_h1.emplace_back(new TH1D("Nall_1", "", 50, 0, 20000));
            v_h1.emplace_back(new TH1D("Nall_2", "", 50, 0, 1000));
            v_h1.emplace_back(new TH1D("Nhit", "", 50, 0, 500));
            v_h1.emplace_back(new TH1D("significance", "", 50, 0, 1000));
            v_h1[0]->SetTitle("Nall_1");
            v_h1[1]->SetTitle("Nall_2");
            v_h1[2]->SetTitle("Nhit");
            v_h1[3]->SetTitle("significance");
            for (auto& p : AffineParam) {
                v_h1[0]->Fill(p.get<picojson::object>().at("Nall_1").get<double>());
                v_h1[1]->Fill(p.get<picojson::object>().at("Nall_2").get<double>());
                v_h1[2]->Fill(p.get<picojson::object>().at("Nhit").get<double>());
                v_h1[3]->Fill(p.get<picojson::object>().at("significance").get<double>());
                va1.emplace_back(p.get<picojson::object>().at("Nall_1").get<double>());
                va2.emplace_back(p.get<picojson::object>().at("Nall_2").get<double>());
                vhit.emplace_back(p.get<picojson::object>().at("Nhit").get<double>());
                vsig.emplace_back(p.get<picojson::object>().at("significance").get<double>());
            }
            for (int i = 0; i < 4; i++) {
                can->cd(i + 1);
                v_h1[i]->Draw();
            }
            can->SaveAs((folder + oname).c_str());

            for (auto& p : v_h1) { delete p; }
        }
        std::sort(vpx.begin(), vpx.end());
        vpx.erase(unique(vpx.begin(), vpx.end()), vpx.end());
        std::sort(vpy.begin(), vpy.end());
        vpy.erase(unique(vpy.begin(), vpy.end()), vpy.end());

        double px_min = *std::min_element(vpx.begin(), vpx.end()) - 5.0 / 6.0 / 2.0;
        double py_min = *std::min_element(vpy.begin(), vpy.end()) - 5.0 / 12.0 / 2.0;
        double px_max = *std::max_element(vpx.begin(), vpx.end()) + 5.0 / 6.0 / 2.0;
        double py_max = *std::max_element(vpy.begin(), vpy.end()) + 5.0 / 12.0 / 2.0;
        {
            vector<TH2D*> v_h2;
            v_h2.emplace_back(new TH2D("ex_map", "", 6, px_min, px_max, 12, py_min, py_max));
            v_h2.emplace_back(new TH2D("ey_map", "", 6, px_min, px_max, 12, py_min, py_max));
            v_h2.emplace_back(new TH2D("dz_map", "", 6, px_min, px_max, 12, py_min, py_max));
            v_h2.emplace_back(new TH2D("e_map", "", 6, px_min, px_max, 12, py_min, py_max));
            v_h2[0]->SetTitle("ex_map"); v_h2[0]->SetStats(false); v_h2[0]->SetMinimum(0.452); v_h2[0]->SetMaximum(0.456);
            v_h2[1]->SetTitle("ey_map"); v_h2[1]->SetStats(false); v_h2[1]->SetMinimum(0.452); v_h2[1]->SetMaximum(0.456);
            v_h2[2]->SetTitle("dz_map"); v_h2[2]->SetStats(false); v_h2[2]->SetMinimum(-5); v_h2[2]->SetMaximum(10);
            v_h2[3]->SetTitle("mag. map"); v_h2[3]->SetStats(false); v_h2[3]->SetMinimum(0.452); v_h2[3]->SetMaximum(0.456);
            SetBinContent(v_h2[0], vpx, vpy, AffineParam, vex);
            SetBinContent(v_h2[1], vpx, vpy, AffineParam, vey);
            SetBinContent(v_h2[2], vpx, vpy, AffineParam, vdz);
            SetBinContent(v_h2[3], vpx, vpy, AffineParam, ve);
            for (int i = 0; i < 4; i++) {
                can->cd(i + 1);
                v_h2[i]->Draw("COLZ");
            }
            can->SaveAs((folder + oname).c_str());

            for (auto& p : v_h2) { delete p; }
        }

        can->SaveAs((folder + oname + "]").c_str(), "Title:graph");
    }
    void MakeOverlap(picojson::array& AffineParam) {
        vector<double> overlap = { 45,60,90,120 };
        const int width = 2048 / 2;
        const int height = 1088 / 2;
        vector<int> px = { width, width, -width, -width };
        vector<int> py = { height , -height ,-height , height };
        vector<double> ax = { 0,5,-5,0,0 };
        vector<double> ay = { 0,0,0,5,-5 };
        for (const auto& p : overlap) {
            auto can = new TCanvas("overlap", "", 400 * 8, 400 * 8);
            auto h = new TH2F("axis", "mosaic sensor position", 3000, -5, 10, 3000, -10, 5);
            h->SetStats(false);
            h->Draw();
            vector<TPolyLine*> vpline;
            for (const auto& q : AffineParam) {
                const auto& Aff_coef = q.get<picojson::object>().at("Aff_coef").get<picojson::array>();
                double a = Aff_coef[0].get<double>();
                double b = Aff_coef[1].get<double>();
                double c = Aff_coef[2].get<double>();
                double d = Aff_coef[3].get<double>();

                for (int j = 0; j < 5; j++) {
                    vector<double> vx, vy;
                    for (int i = 0; i < 5; i++) {
                        double e = px[i % 4] * a + py[i % 4] * b;
                        double f = px[i % 4] * c + py[i % 4] * d;
                        vx.emplace_back(e - (e > 0 ? 1.0 : -1.0) * p * 0.001 / 2 + Aff_coef[4].get<double>() + ax[j]);
                        vy.emplace_back(f - (f > 0 ? 1.0 : -1.0) * p * 0.001 / 2 + Aff_coef[5].get<double>() + ay[j]);
                    }
                    auto pline = new TPolyLine(5, vx.data(), vy.data());
                    pline->SetFillColor(2);
                    pline->SetLineColor(0);
                    pline->SetLineWidth(0);
                    pline->Draw("f");
                    //pline->Draw();
                    vpline.emplace_back(pline);
                }
            }
            can->SaveAs(("mosaic" + std::to_string(p) + ".png").c_str());
            for (auto&& q : vpline) {
                q->Delete();
            }
            h->Delete();
            vpline.clear();
        }
    }
}
int main(int argc, char** argv)
{
    vector<string> strs;
    for (int i = 1; i < argc; i++)
    {
        string str(argv[i]);
        std::transform(str.begin(), str.end(), str.begin(), ::tolower);
        strs.push_back(str);
    }

    bool help = false, pause = false;
    if (std::find(strs.begin(), strs.end(), std::string("-pause")) != strs.end()) { pause = true; }
    if (std::find(strs.begin(), strs.end(), std::string("-help")) != strs.end()) { help = true; }

    if (help) {
        std::cout << "These files are needed at least: \n    ValidViewHistory.json \n    EachViewParam.json \n    AffineParamList.json \n    ScanLinesOrigin.json" << std::endl;

        std::cout << "option1  (引数の必要ないオプション)\n";
        std::cout << "-nopdf        :PDFを作成しない\n";
        std::cout << "-nonot        :Number of tracksを作成しない\n";
        std::cout << "-nodefault    :Defaultの絵を作成しない\n";
        std::cout << "-anglemap     :Angle mapを作成する(一部の視野)\n";
        std::cout << "-phvol        :Ph vol mapを作成する(一部の視野)\n";
        std::cout << "-angleph      :Angle ph (or vol) mapを作成する(一部の視野)\n";
        std::cout << "-processtime  :Process timeを作成する\n";
        std::cout << "-damping      :Dampingを作成する(Results2.jsonが必要)\n";
        std::cout << "-optional     :PDFでOptionalのグラフを作成する\n";
        std::cout << "-pause        :最後にpauseする\n";
        std::cout << "-help         :このヘルプを表示する\n" << std::endl;

        std::cout << "option2 (引数を1個必要とするオプション)\n";
        std::cout << "--affinepath  :アフィンパラメータのJSONファイルのパスを指定する\n";
        std::cout << "--betapath    :betaファイルのパス指定する\n";
        std::cout << "--outputdir   :出力するグラフのディレクトリを指定する(デフォルトはGRAPH)\n";
        std::cout << "--outputabsdir:出力するグラフのディレクトリ(絶対パス)を指定する\n";
        std::cout << "--inputabsdir: 入力するデータのディレクトリ(絶対パス)を指定する\n";
        std::cout << "--direction   :2回取り時の?回目\n";
        std::cout << "--not_up      :NOTのグラフのX軸の上限値を指定する\n";
        std::cout << "--not_up2     :クラスタリング前のNOTのグラフのX軸の上限値を指定する\n";
        std::cout << "--phcut       :Angle mapを作成するときのphcutを指定する\n";
        std::cout << "--format      :テキストフォーマット(.txt)は1、デフォルト(.json)は0\n";
        if (pause) { cin.get(); }
        return -1;
    }

    try {
        bool nopdf = false, nonot = false, nodefault = false, anglemap = false, processtime = false,
            damping = false, optional = false, phvol = false, angleph = false, minimum = false, finez = false;
        if (std::find(strs.begin(), strs.end(), std::string("-nopdf")) != strs.end()) { nopdf = true; }
        if (std::find(strs.begin(), strs.end(), std::string("-nonot")) != strs.end()) { nonot = true; }
        if (std::find(strs.begin(), strs.end(), std::string("-nodefault")) != strs.end()) { nodefault = true; }
        if (std::find(strs.begin(), strs.end(), std::string("-anglemap")) != strs.end()) { anglemap = true; }
        if (std::find(strs.begin(), strs.end(), std::string("-phvol")) != strs.end()) { phvol = true; }
        if (std::find(strs.begin(), strs.end(), std::string("-angleph")) != strs.end()) { angleph = true; }
        if (std::find(strs.begin(), strs.end(), std::string("-processtime")) != strs.end()) { processtime = true; }
        if (std::find(strs.begin(), strs.end(), std::string("-damping")) != strs.end()) { damping = true; }
        if (std::find(strs.begin(), strs.end(), std::string("-optional")) != strs.end()) { optional = true; }
        if (std::find(strs.begin(), strs.end(), std::string("-minimum")) != strs.end()) { minimum = true; }
        if (std::find(strs.begin(), strs.end(), std::string("-finez")) != strs.end()) { finez = true; }

        if (minimum) {
            nodefault = true;
            nonot = true;
            nopdf = true;
            optional = false;
        }

        gSystem->Load("libTree");
        SetMyStyle();

        auto p = std::vector<std::string>::iterator();

        //フォーマット
        int format = 0;
        p = std::find(strs.begin(), strs.end(), std::string("--format"));
        if (p != strs.end() && (p + 1) != strs.end()) { format = std::stoi(*(++p)); }

        auto p1 = std::find(strs.begin(), strs.end(), std::string("--outputdir"));
        auto p2 = std::find(strs.begin(), strs.end(), std::string("--outputabsdir"));

        std::string current_dir;
        {
            TCHAR  current_dir_[255];
            GetCurrentDirectory(sizeof(current_dir_), current_dir_);
            current_dir = std::string(current_dir_);
        }

        if (p1 != strs.end() && p2 != strs.end()) {
            throw std::exception("--outputdir and --outputabsdir");//両方同時に指定できない
        }
        else if (p1 != strs.end() && (p1 + 1) != strs.end()) {
            ofolder = "./" + (*(++p1)) + "/";//相対パス
        }
        else if (p2 != strs.end() && (p2 + 1) != strs.end()) {
            ofolder = (*(++p2)) + "/";//絶対パス
        }
        else { ofolder = "./GRAPH/"; }
        mkdir(ofolder.c_str());

        p = std::find(strs.begin(), strs.end(), std::string("--inputabsdir"));
        if (p != strs.end() && (p + 1) != strs.end()) {
            ifolder = (*(++p)) + "/";
        }
        else { ifolder = current_dir + "/"; }

        std::cout << "Input  folder: " << ifolder << std::endl;
        std::cout << "Output folder: " << ofolder << std::endl;

        p = std::find(strs.begin(), strs.end(), std::string("--affinepath"));
        if (p != strs.end() && (p + 1) != strs.end()) {
            string affineparampath = *(++p);
            picojson::array AffineParam;
            read_json(affineparampath, AffineParam);
            MakeAffineGraph(AffineParam);
            MakeOverlap(AffineParam);
        }
        std::vector<uint64_t> vlist;
        p = std::find(strs.begin(), strs.end(), std::string("--betapath"));
        if (p != strs.end() && (p + 1) != strs.end()) {
            string betafilepath = *(++p);
            read_beta(betafilepath, vlist);
        }

        //パラメータファイルの読み込み
        picojson::array ViewHistory, EachView, Affine, ScanLinesOrigin;
        read_json(ifolder + "AffineParamList.json", Affine);
        read_json(ifolder + "ScanLinesOrigin.json", ScanLinesOrigin);
        if (format == 1) {
            ViewHistory = read_json_arr_from_txt(ifolder + "ValidViewHistory.txt");
            EachView = read_json_arr_from_txt(ifolder + "EachViewParam.txt");
            if (ViewHistory.size() == 0 || EachView.size() == 0)return 0;
        }
        else if (format == 0) {
            read_json(ifolder + "ValidViewHistory.json", ViewHistory);
            read_json(ifolder + "EachViewParam.json", EachView);
        }
        else {
            throw std::exception("Format is not correct.");
        }

        const int window_w = 600;
        const int window_h = 450;

        TCanvas* can = new TCanvas();
        can->SetCanvasSize(window_w * 2, window_h * 3);
        gStyle->SetOptStat("emrou");
        TGaxis::SetMaxDigits(3);

        string oname;

        string Direction;
        p = std::find(strs.begin(), strs.end(), std::string("--direction"));
        if (p != strs.end() && (p + 1) != strs.end()) { Direction = *(++p); }

        double not_up = 0;
        p = std::find(strs.begin(), strs.end(), std::string("--not_up"));
        if (p != strs.end() && (p + 1) != strs.end()) { not_up = std::stod(*(++p)); }

        double not_up2 = 0;
        p = std::find(strs.begin(), strs.end(), std::string("--not_up2"));
        if (p != strs.end() && (p + 1) != strs.end()) { not_up2 = std::stod(*(++p)); }

        int phcut = 0;
        p = std::find(strs.begin(), strs.end(), std::string("--phcut"));
        if (p != strs.end() && (p + 1) != strs.end()) { phcut = std::stoi(*(++p)); }

        double min_thick = 20;
        p = std::find(strs.begin(), strs.end(), std::string("--min_thick"));
        if (p != strs.end() && (p + 1) != strs.end()) { min_thick = std::stod(*(++p)); }

        double max_thick = 120;
        p = std::find(strs.begin(), strs.end(), std::string("--max_thick"));
        if (p != strs.end() && (p + 1) != strs.end()) { max_thick = std::stod(*(++p)); }

        MakeGraph g2(ScanLinesOrigin, Affine, ViewHistory, EachView, window_w, window_h, Direction);

        if (vlist.size() != 0) {
            g2.trygetnot(vlist);
        }
        else {
            if (nonot == false) { g2.trygetnot(); }
        }
        if (minimum) {
            g2.makegraph2(can, ofolder + "nogs0.png", "Nogs", "0", 0, 100 * 1000, 6);
            g2.makegraph2(can, ofolder + "nogs15.png", "Nogs", "15", 0, 100 * 1000, 6);
            g2.makegraph2(can, ofolder + "nog.png", "Nogs", "", 0, 50 * 1000, 6);
            g2.makegraph2(can, ofolder + "picnum6.png", "Bottom-Top", "", 0, 0, 6);
            g2.makegraph2(can, ofolder + "picnum4.png", "Bottom-Top", "", 0, 0, 4);
            g2.makegraph2(can, ofolder + "excount.png", "ImagerControllerParam", "ExposureCount", 0, 1088, 6);
            g2.makegraph1(can, ofolder + "thickness.png", "ScanEachLayerParam", "ThickOfLayer", min_thick, max_thick, 50, 6);
        }
        if (finez) {
            oname = "finez.pdf";
            can->SaveAs((ofolder + oname + "[").c_str(), "Title:graph");
            g2.makegraph3(can, ofolder + oname, "FineZ", "", -25, 25);
            can->SaveAs((ofolder + oname + "]").c_str());
        }
        if (nodefault == false) {

            double not_xup = g2.getavenot(1) * 5;
            double not_uncls_xup = g2.getavenot(2) * 5;
            if (not_up != 0) {
                not_xup = not_up;
            }
            if (not_up2 != 0) {
                not_uncls_xup = not_up2;
            }

            g2.makegraph2(can, ofolder + "nogs0.png", "Nogs", "0", 0, 100 * 1000, 6);
            g2.makegraph2(can, ofolder + "nogs15.png", "Nogs", "15", 0, 100 * 1000, 6);
            g2.makegraph2(can, ofolder + "nog.png", "Nogs", "", 0, 50 * 1000, 6);
            g2.makegraph2(can, ofolder + "picnum6.png", "Bottom-Top", "", 0, 0, 6);
            g2.makegraph2(can, ofolder + "picnum4.png", "Bottom-Top", "", 0, 0, 4);

            g2.makegraph2(can, ofolder + "excount.png", "ImagerControllerParam", "ExposureCount", 0, 1088, 6);
            g2.makegraph2(can, ofolder + "not.png", "# of tracks", "", 0, not_xup, 6);
            g2.makegraph2(can, ofolder + "not_uncls.png", "# of unclustered tracks", "", 0, not_uncls_xup, 6);
            g2.makegraph2(can, ofolder + "startpic.png", "StartAnalysisPicNo", "", 0, 0, 6);
            g2.makegraph1(can, ofolder + "thickness.png", "ScanEachLayerParam", "ThickOfLayer", min_thick, max_thick, 50, 6);
            if (nopdf == true)
            {
                oname = "summary.pdf";
                can->SaveAs((ofolder + oname + "[").c_str(), "Title:graph");
                g2.makegraph2(can, ofolder + oname, "ImagerControllerParam", "ExposureCount", 0, 1088, 6);
                g2.makegraph2(can, ofolder + oname, "Nogs", "0", 0, 100 * 1000, 6);
                g2.makegraph2(can, ofolder + oname, "Nogs", "15", 0, 100 * 1000, 6);
                g2.makegraph2(can, ofolder + oname, "# of tracks", "", 0, not_xup, 6);
                g2.makegraph2(can, ofolder + oname, "# of unclustered tracks", "", 0, not_uncls_xup, 6);
                g2.makegraph2(can, ofolder + oname, "Nogs", "", 0, 100 * 1000, 6);
                g2.makegraph2(can, ofolder + oname, "Nogs", "", 0, 20 * 1000, 6);
                g2.makegraph2(can, ofolder + oname, "Bottom-Top", "", 0, 0, 6);
                g2.makegraph2(can, ofolder + oname, "StartAnalysisPicNo", "", 0, 0, 6);
                g2.makegraph1(can, ofolder + oname, "ScanEachLayerParam", "ThickOfLayer", min_thick, max_thick, 50, 6);
                can->SaveAs((ofolder + oname + "]").c_str());
            }
            //g2.makegraph1(can, folder + "finethickness.png", "ScanEachLayerParam", "FineThickOfLayer", min_thick, max_thick, 50, 4);
        }

        if (optional)
        {
            oname = "optional.pdf";
            can->SaveAs((ofolder + oname + "[").c_str(), "Title:graph");

            g2.makegraph3(can, ofolder + oname, "FineZ", "", -25, 25);
            g2.makegraph3(can, ofolder + oname, "FineZ W/DZ", "", -25, 25);
            g2.makegraph3(can, ofolder + oname, "VeryFineZ", "", -25, 25);

            g2.makegraph2(can, ofolder + oname, "Bottom-Top", "", 0, 0, 6);
            g2.makegraph2(can, ofolder + oname, "Bottom-Top", "", 0, 0, 4);
            g2.makegraph2(can, ofolder + oname, "Bottom", "", 0, 0, 6);
            g2.makegraph2(can, ofolder + oname, "Top", "", 0, 0, 6);
            g2.makegraph1(can, ofolder + oname, "ScanEachLayerParam", "ThickOfLayer", 0, 100, 50, 4);
            g2.makegraph1(can, ofolder + oname, "ScanEachLayerParam", "ThickOfLayer", 40, 80, 50, 4);

            //g2.makegraph1(can, ofolder + oname, "ScanEachLayerParam", "FineThickOfLayer", 0, 100, 50, 4);
            //g2.makegraph1(can, ofolder + oname, "ScanEachLayerParam", "FineThickOfLayer", 40, 100, 50, 4);

            if (nonot == false) {
                g2.makegraph2(can, ofolder + oname, "# of tracks", "", 0, 3000, 6);
                g2.makegraph2(can, ofolder + oname, "# of tracks", "", 0, 10000, 6);
                g2.makegraph2(can, ofolder + oname, "# of unclustered tracks", "", 0, 3 * 1000 * 1000, 6);
            }
            g2.makegraph1(can, ofolder + oname, "SurfaceSummary", "Found", 0, 0, 0, 6);
            g2.makegraph1(can, ofolder + oname, "SurfaceSummary", "Nup", 0, 0, 0, 6);
            g2.makegraph1(can, ofolder + oname, "SurfaceSummary", "Ndown", 0, 0, 0, 6);
            g2.makegraph1(can, ofolder + oname, "SurfaceSummary", "AveOfCenterFound", 0, 0, 0, 6);
            g2.makegraph1(can, ofolder + oname, "SurfaceSummary", "AveOfThickFound", 0, 0, 0, 6);

            g2.makegraph2(can, ofolder + oname, "ImagerControllerParam", "Top5Brightness", 155, 255, 6);
            g2.makegraph2(can, ofolder + oname, "ImagerControllerParam", "ExposureCount", 0, 1088, 6);
            g2.makegraph2(can, ofolder + oname, "ImagerControllerParam", "CorrectionStatus", -50, 50, 6);
            g2.makegraph2(can, ofolder + oname, "Nogs", "", 0, 100 * 1000, 6);
            g2.makegraph2(can, ofolder + oname, "Nogs", "", 0, 20 * 1000, 6);
            g2.makegraph2(can, ofolder + oname, "Nogs", "0", 0, 100 * 1000, 6);
            g2.makegraph2(can, ofolder + oname, "Nogs", "15", 0, 100 * 1000, 6);
            g2.makegraph2(can, ofolder + oname, "StartAnalysisPicNo", "", 0, 0, 6);

            g2.makegraph1(can, ofolder + oname, "ScanEachLayerParam", "DisZ", -200, 200, 50, 6);
            g2.makegraph1(can, ofolder + oname, "ScanEachLayerParam", "FindSurface", -0.5, 1.5, 2, 6);
            g2.makegraph1(can, ofolder + oname, "ScanEachLayerParam", "CorrectedFlag", -0.5, 1.5, 2, 6);
            g2.makegraph1(can, ofolder + oname, "ScanLinesOrigin", "DevZ", -0.2, 0.2, 50, 6);
            g2.makegraph1(can, ofolder + oname, "Z0", "", 0, 500, 50, 6);
            g2.makegraph1(can, ofolder + oname, "Z1", "", 0, 500, 50, 6);
            g2.makegraph1(can, ofolder + oname, "Z1-Z0", "", 0, 200, 50, 6);
            g2.makegraph1(can, ofolder + oname, "RepeatTimes", "", -0.5, 9.5, 10, 4);
            g2.makegraph1(can, ofolder + oname, "StartAnalysisPicNo", "Max-Min+16", 16, 28, 50, 6);

            g2.makegraph1(can, ofolder + oname, "Positions0", "X", -0.5, 0.5, 50, 4);
            g2.makegraph1(can, ofolder + oname, "Positions0", "Y", -0.5, 0.5, 50, 4);
            g2.makegraph1(can, ofolder + oname, "Positions0", "Z", -2, 2, 50, 4);
            g2.makegraph1(can, ofolder + oname, "Positions1", "X", -0.5, 0.5, 50, 4);
            g2.makegraph1(can, ofolder + oname, "Positions1", "Y", -0.5, 0.5, 50, 4);
            g2.makegraph1(can, ofolder + oname, "Positions1", "Z", -0.5, 0.5, 50, 4);

            g2.makegraph1(can, ofolder + oname, "ProcessTimeMain", "ElapsedTime", 0, 1, 50, 6);
            g2.makegraph1(can, ofolder + oname, "DrivingTimeXYZ", "0", 0, 200, 50, 6);
            g2.makegraph2(can, ofolder + oname, "ProcessTimeLoop1", "", 0, 0.5, 6);
            g2.makegraph2(can, ofolder + oname, "MainProcessTime", "", 0, 1, 6);

            can->SaveAs((ofolder + oname + "]").c_str());
        }
        if (anglemap) {
            oname = "anglemap.pdf";
            can->SaveAs((ofolder + oname + "[").c_str());
            g2.makegraph_angle(can, ofolder + oname, -5, 5, 50);
            g2.makegraph_angle(can, ofolder + oname, -20, 20, 40);
            g2.makegraph_angle(can, ofolder + oname, -50, 50, 50);
            g2.makegraph_angle(can, ofolder + oname, -200, 200, 40);
            if (phcut != 0) {
                g2.vangles.clear();
                g2.vangleviews.clear();
                g2.makegraph_angle(can, ofolder + oname, -5, 5, 50, phcut);
                g2.makegraph_angle(can, ofolder + oname, -20, 20, 40, phcut);
                g2.makegraph_angle(can, ofolder + oname, -50, 50, 50, phcut);
                g2.makegraph_angle(can, ofolder + oname, -200, 200, 40, phcut);
            }
            can->SaveAs((ofolder + oname + "]").c_str());
        }
        if (phvol) {
            oname = "phvol.pdf";
            can->SaveAs((ofolder + oname + "[").c_str());
            g2.makegraph_phvol(can, ofolder + oname, 1, 17, 0, 200, 20);
            g2.makegraph_phvol(can, ofolder + oname, 1, 9, 0, 200, 20);
            g2.makegraph_phvol(can, ofolder + oname, 9, 17, 0, 200, 20);
            can->SaveAs((ofolder + oname + "]").c_str());
        }
        if (angleph) {
            oname = "angleph.pdf";
            can->SaveAs((ofolder + oname + "[").c_str());
            g2.makegraph_angleph(can, ofolder + oname, 1, 17, 50);
            g2.makegraph_anglevol(can, ofolder + oname, 0, 200, 50);
            can->SaveAs((ofolder + oname + "]").c_str());
        }
        if (processtime) {
            oname = "processtime.pdf";

            can->SaveAs((ofolder + oname + "[").c_str());

            g2.makegraph2(can, ofolder + oname, "MainProcessTime", "", 0, 1, 6);
            g2.makegraph2(can, ofolder + oname, "MainProcessTime", "", 0, 3, 6);

            g2.makegraph2(can, ofolder + oname, "ProcessTimeImageFilter1", "", 0, 0.3, 6);
            g2.makegraph2(can, ofolder + oname, "ProcessTimeImageFilter1", "", 0, 1.0, 6);

            g2.makegraph1(can, ofolder + oname, "MaxProcessTimeImageFilter1", "", 0, 300, 50, 6);
            g2.makegraph1(can, ofolder + oname, "MaxProcessTimeImageFilter1", "", 0, 1000, 50, 6);

            g2.makegraph1(can, ofolder + oname, "ImagerID of MaxProcessTimeImageFilter1", "", -1, 99, 100, 6, true);
            g2.makegraph1(can, ofolder + oname, "ImagerID of MinProcessTimeImageFilter1", "", -1, 99, 100, 6, true);

            g2.makegraph1(can, ofolder + oname, "DrivingTimeXYZ", "1", 0, 200, 50, 6);
            g2.makegraph1(can, ofolder + oname, "DrivingTimeXYZ", "2", 0, 200, 50, 6);
            g2.makegraph1(can, ofolder + oname, "DrivingTimePiezo", "", 0, 50, 50, 6);

            /*
            g2.makegraph1(can, ofolder + oname, "ProcessTimeMain", "Process 0", 0, 500, 50, 6);
            g2.makegraph1(can, ofolder + oname, "ProcessTimeMain", "Process 1", 0, 500, 50, 6);
            g2.makegraph1(can, ofolder + oname, "ProcessTimeMain", "Process 2", 0, 500, 50, 6);
            g2.makegraph1(can, ofolder + oname, "ProcessTimeMain", "Process 3", 0, 500, 50, 6);
            g2.makegraph1(can, ofolder + oname, "ProcessTimeMain", "Process 4", 0, 500, 50, 6);
            g2.makegraph1(can, ofolder + oname, "ProcessTimeMain", "Process 5", 0, 500, 50, 6);
            g2.makegraph1(can, ofolder + oname, "ProcessTimeMain", "Process 6", 0, 500, 50, 6);
            g2.makegraph1(can, ofolder + oname, "ProcessTimeMain", "Process 7", 0, 500, 50, 6);
            g2.makegraph1(can, ofolder + oname, "ProcessTimeMain", "Process 8", 0, 500, 50, 6);
            g2.makegraph1(can, ofolder + oname, "ProcessTimeMain", "Process 9", 0, 500, 50, 6);

            g2.makegraph1(can, ofolder + oname, "ProcessTimeMain", "Process 0", 0, 50, 50, 6);
            g2.makegraph1(can, ofolder + oname, "ProcessTimeMain", "Process 1", 0, 50, 50, 6);
            g2.makegraph1(can, ofolder + oname, "ProcessTimeMain", "Process 2", 0, 50, 50, 6);
            g2.makegraph1(can, ofolder + oname, "ProcessTimeMain", "Process 3", 0, 50, 50, 6);
            g2.makegraph1(can, ofolder + oname, "ProcessTimeMain", "Process 4", 0, 50, 50, 6);
            g2.makegraph1(can, ofolder + oname, "ProcessTimeMain", "Process 5", 0, 50, 50, 6);
            g2.makegraph1(can, ofolder + oname, "ProcessTimeMain", "Process 6", 0, 50, 50, 6);
            g2.makegraph1(can, ofolder + oname, "ProcessTimeMain", "Process 7", 0, 50, 50, 6);
            g2.makegraph1(can, ofolder + oname, "ProcessTimeMain", "Process 8", 0, 50, 50, 6);
            g2.makegraph1(can, ofolder + oname, "ProcessTimeMain", "Process 9", 0, 50, 50, 6);*/
            can->SaveAs((ofolder + oname + "]").c_str());
        }
        if (damping) {
            picojson::array Results2;
            read_json(ifolder + "Results2.json", Results2);

            oname = "damping1.pdf";
            can->SaveAs((ofolder + oname + "[").c_str());
            can->SaveAs((ofolder + oname).c_str());
            g2.makedamping1(can, ofolder + oname, Results2);
            can->SaveAs((ofolder + oname + "]").c_str());

            oname = "damping2.pdf";
            can->SaveAs((ofolder + oname + "[").c_str());
            can->SaveAs((ofolder + oname).c_str());
            g2.makedamping2(can, ofolder + oname, Results2);
            can->SaveAs((ofolder + oname + "]").c_str());
        }
        g2.getsummary(ofolder + "summary.txt");
    }
    catch (std::exception& ex)
    {
        cout << ex.what() << endl;
    }
    catch (...)
    {
        cout << "unknown error." << endl;
    }
    if (pause) { std::cout << "Press Any key to continue..." << std::endl; std::cin.get(); }
#if _DEBUG
    _CrtDumpMemoryLeaks();
#endif
    return 0;
}
