import json
import os
import math
import copy

def get_view(x,y,l,vjson):
        cand = []
        for i,js in enumerate(vjson):
            if js["LayerID"]==l and math.isclose(js["Stage_x"],x) and math.isclose(js["Stage_y"],y):
                cand.append(i)
        assert(len(cand)==1)
        return cand[0]

def param_OneThird2normal():
    vname = "Beta_EachViewParam.json"
    sname = "Beta_EachShotParam.json"
    iname = "Beta_EachImagerParam.json"
    vname0 = "Beta_EachViewParam_org.json"
    sname0 = "Beta_EachShotParam_org.json"
    iname0 = "Beta_EachImagerParam_org.json"

    if os.path.exists(vname0):
            print("変換済みの可能性があるので処理を中断します")
            return

    # アルゴリズムの確認
    with open("ScanAreaParam.json") as f:
        js = json.load(f)
        if js["Algorithm"]!="One_Third_Half_HTS2":
            print("ScanAreaParam.AlgorithmがOne_Third_Half_HTS2ではないので処理を中断します")
            return

    # ファイル名を変更
    os.rename(vname,vname0)
    os.rename(sname,sname0)
    os.rename(iname,iname0)

    sjson = json.load(open(sname0))
    ijson = json.load(open(iname0))
    vjson = json.load(open(vname0))


    # ステージ座標が格子状であることの確認
    xs = sorted(list(set([s["Stage_x"] for s in vjson])))
    ys = sorted(list(set([s["Stage_y"] for s in vjson])))
    cols = len(xs)
    rows = len(ys)//3
    assert(cols*rows*3*2==len(vjson))
    stepx = (xs[-1]-xs[0])/(cols-1)
    stepy = (ys[-1]-ys[0])/(rows*9-7)
    assert(math.isclose((xs[1]-xs[0]),stepx))
    assert(math.isclose((ys[1]-ys[0]),stepy))
    assert(24==len(ijson))


    sjson2 = []
    vjson2 = []
    ijson2 = []


    for row in range(rows):
        for yi in range(3):
            for l in range(2):
                for col in range(cols):
                    for ii in range(24):
                        ishot = ii + 24*col + 24*cols*l + 24*cols*2*yi + 24*cols*2*3*row
                        view = sjson[ishot]["View"]
                        x = vjson[view]["Stage_x"]
                        y = vjson[view]["Stage_y"]-stepy*yi
                        new_view = get_view(x,y,l,vjson)
                        js = copy.deepcopy(sjson[ishot])
                        if js["View"] != new_view:
                            js["View"] = new_view
                            js["Imager"] = js["Imager"]+24*yi
                        sjson2.append(js)

    for yi in range(3):
        for ii in range(24):
            js = copy.deepcopy(ijson[ii])
            js["ImagerID"] +=24*yi
            js["CameraID"] +=2*yi
            js["GridY"] += yi
            js["Aff_coef"][5]+=stepy*yi
            ijson2.append(js)


    for row in range(rows):
        for yi in range(3):
            for l in range(2):
                for col in range(cols):
                    iview = col + cols*l + cols*2*yi + cols*2*3*row
                    js = copy.deepcopy(vjson[iview])
                    js["Z_begin"]=None
                    js["Z_end"]=None
                    if yi==0:
                        iview0 = col + cols*l + cols*2*0 + cols*2*3*row
                        iview1 = col + cols*l + cols*2*1 + cols*2*3*row
                        iview2 = col + cols*l + cols*2*2 + cols*2*3*row
                        js["Z_begin0"]=vjson[iview0]["Z_begin"]
                        js["Z_begin1"]=vjson[iview1]["Z_begin"]
                        js["Z_begin2"]=vjson[iview2]["Z_begin"]
                        js["Z_end0"]=vjson[iview0]["Z_end"]
                        js["Z_end1"]=vjson[iview1]["Z_end"]
                        js["Z_end2"]=vjson[iview2]["Z_end"]
                    else:
                        js["Comment"]="not used"
                    vjson2.append(js)

    with open(vname,"w") as f:json.dump(vjson2,f,indent=1)
    with open(sname,"w") as f:json.dump(sjson2,f,indent=1)
    with open(iname,"w") as f:json.dump(ijson2,f,indent=1)
    print("変換成功")

param_OneThird2normal()
