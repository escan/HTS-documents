import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import json
from matplotlib.backends.backend_pdf import PdfPages
from statistics import mean,stdev
from scipy.optimize import curve_fit

def bins2x(bins: list):
    out = []
    for i in range(len(bins) - 1):
        out.append((bins[i] + bins[i+1]) / 2)
    return out

def func(x, a, mu, sigma):
    return a * np.exp(-(x-mu)**2/(2*sigma**2))

def plot_beta_ali():
    target_file = "ali.json"
    graph_path = "ali.pdf"

    with open(target_file) as f:
        obj = json.load(f)
    pdf = PdfPages(graph_path)
    plt.clf()
    plt.cla()

    if "shift_before" not in obj:
        max_x = +5
        min_x = -5
        max_y = +5
        min_y = -5
    else:
        sub_obj = obj["shift_before"]
        max_x = sub_obj["max_x"] * 1000
        min_x = sub_obj["min_x"] * 1000
        max_y = sub_obj["max_y"] * 1000
        min_y = sub_obj["min_y"] * 1000

    for i, (shift,title) in enumerate(zip(["shift_before","shift_after"],["Before alignment","After alignment"])):
        if shift not in obj:continue
        sub_obj = obj[shift]
        nbin_x = int(sub_obj["nbin_x"])
        nbin_y = int(sub_obj["nbin_y"])
        binw_x = (max_x - min_x) / nbin_x
        binw_y = (max_y - min_y) / nbin_y
        arr = sub_obj["array"]
        vx = []
        vy = []
        for iy in range(nbin_y):
            dy = min_y + binw_y * (iy + 0.5)
            for ix in range(nbin_x):
                dx = min_x + binw_x * (ix + 0.5)
                for i in range(arr[iy][ix]):
                    vx.append(dx)
                    vy.append(dy)
        import matplotlib as mpl
        plt.hist2d(vx,vy, bins=[nbin_x,nbin_y],range=[[min_x,max_x],[min_y,max_y]],cmap=cm.inferno)
        plt.title(title + ' (linear)')
        plt.colorbar()
        plt.xlabel("Shift X [um]")
        plt.ylabel("Shift Y [um]")
        pdf.savefig()
        plt.clf()

        plt.hist2d(vx,vy, bins=[nbin_x,nbin_y],range=[[min_x,max_x],[min_y,max_y]],norm=mpl.colors.LogNorm(),cmap=cm.inferno)
        plt.title(title + ' (log)')
        plt.colorbar()
        plt.xlabel("Shift X [um]")
        plt.ylabel("Shift Y [um]")
        pdf.savefig()
        plt.clf()

        hist_x = [x for x,y in zip(vx,vy) if abs(y)<=binw_y*2]
        histreturn = plt.hist(hist_x, bins=nbin_x, range=[min_x,max_x], color='r')
        param_ini = [max(histreturn[0]), mean(hist_x), stdev(hist_x)]
        X = bins2x(histreturn[1])
        Y = histreturn[0]
        popt, pcov = curve_fit(func, X, Y, p0=param_ini)
        fitting = func(np.linspace(min_x, max_x, 200), popt[0], popt[1], popt[2])
        plt.plot(np.linspace(min_x, max_x, 200), fitting, '-', color=('k'))
        text = 'Entries: {:d}\nMean: {:.4g}\nStd Dev: {:.4g}\nHeight: {:.4g}\nCenter: {:.4}\nsigma: {:.4g}'.format(len(hist_x), mean(hist_x), stdev(hist_x), popt[0], popt[1], popt[2])
        print(title + ' X\n' + text)
        plt.text(max_x*0.7, max(Y)*0.9, text, bbox=(dict(boxstyle='square', fc='w')))
        plt.title(title+" X")
        plt.xlabel("Shift X [um]")
        pdf.savefig()
        plt.clf()

        hist_y = [y for x,y in zip(vx,vy) if abs(x)<=binw_x*2]
        histreturn = plt.hist(hist_y, bins=nbin_y, range=[min_y,max_y], color='b')
        param_ini = [max(histreturn[0]), mean(hist_y), stdev(hist_y)]
        X = bins2x(histreturn[1])
        Y = histreturn[0]
        popt, pcov = curve_fit(func, X, Y, p0=param_ini)
        fitting = func(np.linspace(min_y, max_y, 200), popt[0], popt[1], popt[2])
        plt.plot(np.linspace(min_y, max_y, 200), fitting, '-', color=('k'))
        text = 'Entries: {:d}\nMean: {:.4g}\nStd Dev: {:.4g}\nHeight: {:.4g}\nCenter: {:.4}\nsigma: {:.4g}'.format(len(hist_y), mean(hist_y), stdev(hist_y), popt[0], popt[1], popt[2])
        print(title + ' Y\n' + text)
        plt.text(max_y*0.7, max(Y)*0.9, text, bbox=(dict(boxstyle='square', fc='w')))
        plt.title(title+" Y")
        plt.xlabel("Shift Y [um]")
        pdf.savefig()
        plt.clf()

    imagers = []
    xys = [[],[]]
    xy_title = ["X","Y"]
    lstyles = ["--","-"]
    for i, imager in enumerate(obj["ali_imager"]) :
        imagers.append(i)
        xys[0].append(imager["Aff_coef_offset"][4] * 1000)
        xys[1].append(imager["Aff_coef_offset"][5] * 1000)

    for xy in range(2):
        plt.plot(imagers, xys[xy], label=xy_title[xy] + "-axis", linestyle=lstyles[xy])
        plt.ylim(min_y - 5,max_y + 5)
        plt.text(len(xys[0]) / 2 * 0.65, max_y * 1.1, "Calculation limit", fontsize=12)
        plt.axhline(y=min_y,linestyle="--",color="black")
        plt.axhline(y=max_y,linestyle="--",color="black")
    plt.xlabel("Imager ID")
    plt.ylabel("Shift [um]")
    plt.legend()
    pdf.savefig()
    plt.clf()

    sigs = [pair["significance"] for pair in obj["ali_imager_pair"]]
    plt.hist(sigs,range=[0,100],bins=20)
    plt.title("Significance")
    pdf.savefig()
    plt.clf()
    pdf.close()
    print(graph_path,"output")

    
import sys
import os
if __name__ == "__main__":
    original_dir = os.getcwd()
    if len(sys.argv)==2:
        target_path = sys.argv[1]
        if os.path.isfile(target_path):
            target_dir = os.path.dirname(target_path)
        elif os.path.isdir(target_path):
            target_dir = os.path.abspath(target_path)
        else:raise
        print("Target:",target_dir)
        os.chdir(target_dir)
    elif len(sys.argv)==1:
        print("Target:",os.path.abspath(original_dir))
    else:raise
    plot_beta_ali()
    os.chdir(original_dir)
