def param_half2normal():
    import json
    import os
    import math
    import copy
    vname = "Beta_EachViewParam.json"
    sname = "Beta_EachShotParam.json"
    iname = "Beta_EachImagerParam.json"
    vname0 = "Beta_EachViewParam.json.org"
    sname0 = "Beta_EachShotParam.json.org"
    iname0 = "Beta_EachImagerParam.json.org"

    if os.path.exists(vname0):
        print("変換済みの可能性があるので処理を中断します")
        return

    # アルゴリズムの確認
    with open("ScanAreaParam.json") as f:
        js = json.load(f)
        if js["Algorithm"]!="Half_Half_Zigzag_HTS":
            print("ScanAreaParam.AlgorithmがHalf_Half_Zigzag_HTSではないので処理を中断します")
            return
    
    # ファイル名を変更
    os.rename(vname,vname0)
    os.rename(sname,sname0)
    os.rename(iname,iname0)

    # JSONファイルの読み取り
    with open(vname0) as f:vjson = json.load(f)
    with open(sname0) as f:sjson = json.load(f)
    with open(iname0) as f:ijson = json.load(f)

    # ステージ位置が正しいか確認
    for i in range(0,len(vjson),2):
        assert(math.isclose(abs(vjson[i]["Stage_x"]-vjson[i+1]["Stage_x"]),5/6)) # 5mm / 6センサ分
    
    # 新パラメータの生成
    sjson2 = []
    vjson2 = []
    ijson2 = []

    # ShotとViewのJSON作成
    for i in range(0,len(sjson),72):
        view0 = sjson[i]["View"] # 前半36センサ
        view1 = view0+1          # 後半36センサ
        assert(view1 == sjson[i+36]["View"])
        
        js0 = copy.deepcopy(vjson[view0])
        js1 = copy.deepcopy(vjson[view1])
        
        # 視野ごとに2つのZ座標を設定できないので特別なパラメータに
        # beta処理系のアップデートが必要
        layer = js0["LayerID"]
        if layer==0:
            # layer==0は前半→後半の順でスキャンしている
            js0["Z_begin0"]=js0["Z_begin"]
            js0["Z_end0"]=js0["Z_end"]
            js0["Z_begin1"]=js1["Z_begin"]
            js0["Z_end1"]=js1["Z_end"]
            js1["Comment"]="not used"
            for j in range(72):
                js = copy.deepcopy(sjson[i+j])
                if j>=36:
                    js["View"] = view0
                    js["Imager"] = js["Imager"]+36
                sjson2.append(js)
        elif layer==1:
            # layer==1は後半→前半の順でスキャンしている
            js1["Z_begin0"]=js1["Z_begin"]
            js1["Z_end0"]=js1["Z_end"]
            js1["Z_begin1"]=js0["Z_begin"]
            js1["Z_end1"]=js0["Z_end"]
            js0["Comment"]="not used"
            for j in range(72):
                js = copy.deepcopy(sjson[i+j])
                if j<36:
                    js["View"] = view1
                    js["Imager"] = js["Imager"]+36
                sjson2.append(js)
        else:raise
        
        js0["Z_begin"]=None
        js0["Z_end"]=None
        js1["Z_begin"]=None
        js1["Z_end"]=None
        vjson2.append(js0)
        vjson2.append(js1)
        
    # ImagerのJSON作成
    for i in range(36):
        js = copy.deepcopy(ijson[i])
        ijson2.append(js)
    for i in range(36):
        js = copy.deepcopy(ijson[i])
        js["GridX"]+=1     # Grid=センサの配置パラメータ 整数は+1
        js["CameraID"]+=3  # カメラIDは3つ足す
        js["ImagerID"]+=36 # Imagerは+36する
        js["Aff_coef"][4]+=5/6  # 5mm / 6センサ分
        ijson2.append(js)

    # 新パラメータの出力
    with open(vname,"w") as f:json.dump(vjson2,f,indent=1)
    with open(sname,"w") as f:json.dump(sjson2,f,indent=1)
    with open(iname,"w") as f:json.dump(ijson2,f,indent=1)
    print("変換成功")
    
param_half2normal()
